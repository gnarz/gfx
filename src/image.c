/* image.c
 *
 * image and texture handling for gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"
#include <errno.h>

/*** Module
 * Name: gfx
 */

#define STBI_FAILURE_USERMSG
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

static const char *texture_names[10] = {
	WINDOW_PROP_TEXTURE "0",
	WINDOW_PROP_TEXTURE "1",
	WINDOW_PROP_TEXTURE "2",
	WINDOW_PROP_TEXTURE "3",
	WINDOW_PROP_TEXTURE "4",
	WINDOW_PROP_TEXTURE "5",
	WINDOW_PROP_TEXTURE "6",
	WINDOW_PROP_TEXTURE "7",
	WINDOW_PROP_TEXTURE "8",
	WINDOW_PROP_TEXTURE "9"
};

/*** Object
 * Name: image
 */

void gfx_imageDelete(GLuint id)
{
	glDeleteTextures(1, &id);
}

int gfx_imageSize(gfx_image *img, int *w, int *h, int *d)
{
	glBindTexture(GL_TEXTURE_2D, img->id);
	if (w) {
		glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, w);
	}
	if (h) {
		glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, h);
	}
	if (d) {
		int i;
		glGetTexLevelParameteriv(GL_TEXTURE_2D, 0,  GL_TEXTURE_INTERNAL_FORMAT, &i);
		switch (i) {
			case GL_RED: case GL_R8: *d = 1; break;
			case GL_RGBA: case GL_RGBA8: *d = 4; break;
			default: *d = 0;
		}
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	return 1;
}

static void gfx_imageFlip(uint8_t *buf, int width, int height, int depth)
{
	// flip vertically
	int rowlen = width * depth, y;
	uint8_t rowbuf[rowlen];
	for (y = 0; y < height / 2; ++y) {
		uint8_t *r0 = &buf[y * rowlen];
		uint8_t *r1 = &buf[(height - y - 1) * rowlen];
		memcpy(rowbuf, r0, rowlen);
		memcpy(r0, r1, rowlen);
		memcpy(r1, rowbuf, rowlen);
	}
}

#define gfx_pushImage(L) gfx_pushGLObject(L, GFX_IMAGE, gfx_imageDelete)

/*** Method
 * Object: image
 * Name: getsize
 * Synopsis: w, h, d = image:getsize()
 */
static int gfx_image_getsize(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_image *img = gfx_checkImage(L, 1);
	int w, h, d;
	gfx_imageSize(img, &w, &h, &d);
	lua_pushinteger(L, w);
	lua_pushinteger(L, h);
	lua_pushinteger(L, d);
	return 3;
}

/*** Method
 * Object: image
 * Name: save
 * Synopsis: image:save(path)
 */
static int gfx_image_save(lua_State *L)
{
	gfx_check_nargs(1, 1, 1);
	gfx_image *img = gfx_checkImage(L, 1);
	const char *path = luaL_checkstring(L, 2);

	int width, height, depth;
	gfx_imageSize(img, &width, &height, &depth);
	if (width == 0 || height == 0 || depth == 0) {
		return luaL_error(L, "save failed: invalid image");
	}

	int rowlen = width * depth;
	uint8_t *buf = malloc(width * height * depth);
	assert(buf != NULL);
	glBindTexture(GL_TEXTURE_2D, img->id);
	glGetTexImage(GL_TEXTURE_2D, 0, depth == 1 ? GL_RED : GL_RGBA, GL_UNSIGNED_BYTE, buf);
	glBindTexture(GL_TEXTURE_2D, 0);
	gfx_checkGLerror(L, "save failed");

	gfx_imageFlip(buf, width, height, depth);

	int ok = 0;
	errno = 0;
	const char *ext = strrchr(path, '.');
	if (ext && strcmp(ext, ".png") == 0) {
		ok = stbi_write_png(path, width, height, depth, buf, rowlen);
	} else if (ext && strcmp(ext, ".bmp") == 0) {
		ok = stbi_write_bmp(path, width, height, depth, buf);
	} else if (ext && strcmp(ext, ".tga") == 0) {
		ok = stbi_write_tga(path, width, height, depth, buf);
	} else {
		return luaL_error(L, "save failed: invalid output file format");
	}

	if (!ok) {
		return luaL_error(L, "save failed: %s", errno ? strerror(errno) : "invalid size");
	}

	return 0;
}

static const luaL_Reg gfx_image_methods[] = {
	{"getsize", gfx_image_getsize},
	{"save", gfx_image_save},
	{0, 0}
};

int gfx_imageUpdate(gfx_image *img, int w, int h, uint8_t *data)
{
	int format;
	gfx_imageSize(img, NULL, NULL, &format);
	format = format == 1 ? GL_RED : GL_RGBA;
	glBindTexture(GL_TEXTURE_2D, img->id);
	glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, GL_UNSIGNED_BYTE, data);
	glBindTexture(GL_TEXTURE_2D, 0);
	return 1;
}

static GLuint gfx_createTexture(int width, int height, int chan, uint8_t *data)
{
	int format;
	switch (chan) {
		case 1: format = GL_RED; break;
		case 4: format = GL_RGBA; break;
		default: return 0;
	}

	GLuint id;
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);

	return id;
}

int gfx_pushNewImage(lua_State *L, int width, int height, int chan, uint8_t *data)
{
	gfx_image *img = gfx_pushImage(L);
	img->id = gfx_createTexture(width, height, chan, data);
	gfx_checkGLerror(L, "image creation failed");
	return 1;
}

/*** Constructor
 * Object: image
 * Name: loadimage
 * Synopsis: image = gfx:loadimage(name)
 */
static int gfx_loadimage(lua_State *L)
{
	gfx_check_nargs(0, 1, 1);
	const char *name = luaL_checkstring(L, 1);
	int width, height, n;
	uint8_t *data = stbi_load(name, &width, &height, &n, 4);
	if (!data) {
		return luaL_error(L, "loadimage failed: %s", stbi_failure_reason());
	}
	gfx_imageFlip(data, width, height, 4);
	gfx_image *img = gfx_pushImage(L);
	img->id = gfx_createTexture(width, height, 4, data);
	gfx_checkGLerror(L, "loadimage failed");
	if (!img->id) {
		stbi_image_free(data);
		return luaL_error(L, "loadimage failed");
	}

	stbi_image_free(data);
	return 1;
}

/*** Constructor
 * Object: image
 * Name: image
 * Synopsis: image = gfx:image(w, h, depth = 4)
 * depth may be 1 or 4
 */
static int gfx_newimage(lua_State *L)
{
	gfx_check_nargs(0, 2, 3);
	int width = luaL_checkinteger(L, 1);
	int height = luaL_checkinteger(L, 2);
	int depth = luaL_optinteger(L, 3, 4);
	gfx_image *img = gfx_pushImage(L);
	img->id = gfx_createTexture(width, height, depth, 0);
	gfx_checkGLerror(L, "image creation failed");
	if (!img->id) {
		return luaL_error(L, "image creation failed");
	}
	return 1;
}

static const luaL_Reg gfx_image_funcs[] = {
	{"loadimage", gfx_loadimage},
	{"image", gfx_newimage},
	{0, 0}
};

/*** Method
 * Object: window
 * name: settexture
 * Synopsis: window:settexture(texture = nil, unit = 0)
 */
static int gfx_window_settexture(lua_State *L)
{
	gfx_check_nargs(1, 0, 2);
	int unit = 0;
	gfx_checkAndUseWindow(L, 1);
	if (lua_gettop(L) > 1) {
		if (!lua_isnil(L, 2)) {
			gfx_checkImage(L, 2);
		}
		unit = luaL_optinteger(L, 3, 0);
	} else {
		lua_pushnil(L);
	}

	gfx_setProperty(L, 1, 2, texture_names[unit]);
	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * name: gettexture
 * Synopsis: shader = window:gettexture(id = 0)
 */
static int gfx_window_gettexture(lua_State *L)
{
	gfx_check_nargs(1, 0, 1);
	gfx_checkAndUseWindow(L, 1);
	int unit = luaL_optinteger(L, 2, 0);
	if (unit < 0 || unit > 9) {
		return luaL_argerror(L, 2, "argument must be in the range from 0 to 9");
	}
	gfx_getProperty(L, 1, texture_names[unit]);
	return 1;
}

static GLuint gfx_getTextureId(lua_State *L, int idx, int unit)
{
	gfx_checkAndUseWindow(L, idx);
	if (gfx_getProperty(L, 1, texture_names[unit]) != LUA_TNIL) {
		gfx_image *img = gfx_checkImage(L, -1);
		lua_pop(L, 1);
		return img->id;
	}
	lua_pop(L, 1);
	return 0;
}

void gfx_setupTextures(lua_State *L, int idx)
{
	GLuint shid = gfx_getShaderId(L, idx);
	for (int unit = 0; unit < 10; ++unit) {
		GLuint txid = gfx_getTextureId(L, idx, unit);
		glActiveTexture(GL_TEXTURE0 + unit);
		if (txid) {
			glBindTexture(GL_TEXTURE_2D, txid);
			gfx_shaderSetUniformInt(shid, texture_names[unit], unit);
		} else {
			glBindTexture(GL_TEXTURE_2D, 0);
		}
	}
	glActiveTexture(GL_TEXTURE0);
}

/*** Method
 * Object: window
 * Name: grabimage
 * Synopsis: window:grabimage(x, y, w, h)
 */
static int gfx_window_grabimage(lua_State *L)
{
	gfx_check_nargs(1, 0, 4);
	(void) gfx_checkAndUseWindow(L, 1);

	int x, y, w, h;
	GLint vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);

	if (lua_gettop(L) >= 3) {
		x = luaL_checkinteger(L, 2);
		y = luaL_checkinteger(L, 3);
	} else {
		x = vp[0];
		y = vp[1];
	}
	if (lua_gettop(L) > 3) {
		w = luaL_checkinteger(L, 4);
		h = luaL_checkinteger(L, 5);
	} else {
		w = vp[2];
		h = vp[3];
	}

	y = vp[3] - y - h;

	unsigned char* buf = malloc(w * h * 4);
	assert(buf != NULL);
	glReadPixels(x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, buf);
	gfx_checkGLerror(L, "grabimage failed");
	gfx_image *img = gfx_pushImage(L);
	img->id = gfx_createTexture(w, h, 4, buf);
	free(buf);

	if (!img->id) {
		return luaL_error(L, "grabimage failed");
	}
	return 1;
}

/*** Method
 * Object: window
 * Name: putimage
 * Synopsis: window:putimage(image, x = 0, y = 0)
 */
static int gfx_window_putimage(lua_State *L)
{
	gfx_check_nargs(1, 1, 3);
	(void) gfx_checkAndUseWindow(L, 1);
	gfx_image *img = gfx_checkImage(L, 2);
	float x = luaL_optnumber(L, 3, 0);
	float y = luaL_optnumber(L, 4, 0);

	gfx_vec2 coords[6], texcs[6];
	int w, h;
	gfx_imageSize(img, &w, &h, NULL);
	gfx_rect coord_r = {x, y, w, h};
	gfx_rect texc_r = {0, 0, 1, 1};
	gfx_makeTexturedRectangle(&coord_r, &texc_r, coords, texcs);
	gfx_getProperty(L, 1, texture_names[0]);
	int oldtex = lua_gettop(L);
	gfx_setProperty(L, 1, 2, texture_names[0]);
	gfx_drawstuff(L, 1, GL_TRIANGLES, 6, coords, texcs, NULL);
	gfx_setProperty(L, 1, oldtex, texture_names[0]);
	lua_pushvalue(L, 1);
	return 1;
}

static const luaL_Reg gfx_window_image_methods[] = {
	{"settexture", gfx_window_settexture},
	{"gettexture", gfx_window_gettexture},
	{"grabimage", gfx_window_grabimage},
	{"putimage", gfx_window_putimage},
	{0, 0}
};

int gfx_registerImageFunctions(lua_State *L)
{
	gfx_newGLObject(L, GFX_IMAGE, gfx_image_methods);

	luaL_setfuncs(L, gfx_image_funcs, 0);
	return 1;
}

int gfx_registerImageMethods(lua_State *L)
{
	luaL_setfuncs(L, gfx_window_image_methods, 0);
	return 1;
}
