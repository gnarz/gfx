/* draw.c
 *
 * drawing functions for gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"

/*** Module
 * Name: gfx
 */

/*** Object
 * Name: matrix
 * just a wrapper object for a transformation matrix
 */
#define gfx_pushMatrix(L) (gfx_matrix*)gfx_pushHelper(L, GFX_MATRIX, sizeof(gfx_matrix))
#define gfx_checkMatrix(L, idx) (gfx_matrix*)luaL_checkudata(L, idx, GFX_MATRIX)

/*** Module
 * Name: gfx
 */

#define gfx_flushIfImmediate(win) do { if (win->mode == DIRECT) { glFlush(); } } while (0)

void gfx_makeTexturedRectangle(const gfx_rect *r, const gfx_rect *t, gfx_vec2 *coords, gfx_vec2 *texcs)
{
	static const gfx_rect dposrect = {0, 0, 2, 2};
	static const gfx_rect dtexrect = {0, 0, 1, 1};

	if (!r) {
		r = &dposrect;
	}
	if (!t) {
		t = &dtexrect;
	}

	coords[0] = (gfx_vec2) {r->x, r->y};
	coords[1] = (gfx_vec2) {r->x, r->y + r->h-1};
	coords[2] = (gfx_vec2) {r->x + r->w-1, r->y + r->h-1};
	coords[3] = (gfx_vec2) {r->x, r->y};
	coords[4] = (gfx_vec2) {r->x + r->w-1, r->y + r->h-1};
	coords[5] = (gfx_vec2) {r->x + r->w-1, r->y};

	texcs[0] = (gfx_vec2) {t->x, t->y + t->h};
	texcs[1] = (gfx_vec2) {t->x, t->y};
	texcs[2] = (gfx_vec2) {t->x + t->w, t->y};
	texcs[3] = (gfx_vec2) {t->x, t->y + t->h};
	texcs[4] = (gfx_vec2) {t->x + t->w, t->y};
	texcs[5] = (gfx_vec2) {t->x + t->w, t->y + t->h};
}

/*** Method
 * Object: window
 * Name: clear
 * Synopsis: window:clear() or window:clear(r, g, b)
 */
static int gfx_clear(lua_State *L)
{
	gfx_check_nargs(1, 0, 3);
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	gfx_color col = win->bgcolor;
	if (lua_gettop(L) > 2) {
		col.r = luaL_checknumber(L, 2);
		col.g = luaL_checknumber(L, 3);
		col.b = luaL_checknumber(L, 4);
	}
	glClearColor(col.r, col.g, col.b, 1);
	glClear(GL_COLOR_BUFFER_BIT);
	if (win->mode == DIRECT) {
		glFlush();
	}
	lua_pushvalue(L, 1);
	return 1;
}

static int gfx_setcolor(lua_State *L, gfx_color *col)
{
	gfx_check_nargs(1, 3, 4);
	col->r = luaL_checknumber(L, 2);
	col->g = luaL_checknumber(L, 3);
	col->b = luaL_checknumber(L, 4);
	col->a = luaL_optnumber(L, 5, 1.0);
	lua_pushvalue(L, 1);
	return 1;
}

static int gfx_getcolor(lua_State *L, gfx_color *col)
{
	gfx_check_nargs(1, 0, 0);
	lua_pushnumber(L, col->r);
	lua_pushnumber(L, col->g);
	lua_pushnumber(L, col->b);
	lua_pushnumber(L, col->a);
	return 4;
}

/*** Method
 * Object: window
 * Name: setfg
 * Synopsis: window:setfg(r, g, b, a = 1)
 */
static int gfx_setfg(lua_State *L)
{
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	return gfx_setcolor(L, &win->fgcolor);
}

/*** Method
 * Object: window
 * Name: getfg
 * Synopsis: r, g, b, a = window:getfg()
 */
static int gfx_getfg(lua_State *L)
{
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	return gfx_getcolor(L, &win->fgcolor);
}

/*** Method
 * Object: window
 * Name: setbg
 * Synopsis: window:setbg(r, g, b, a = 1) or window:setbg({r = r, g = g, b = b, a = a})
 */
static int gfx_setbg(lua_State *L)
{
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	return gfx_setcolor(L, &win->bgcolor);
}

/*** Method
 * Object: window
 * Name: getbg
 * Synopsis: {r,g,b,a} = window:getbg()
 */
static int gfx_getbg(lua_State *L)
{
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	return gfx_getcolor(L, &win->bgcolor);
}

/*** Method
 * Object: window
 * Name: scale
 * Synopsis: window:scale(xscale, yscale = xscale)
 */
static int gfx_scale(lua_State *L)
{
	gfx_check_nargs(1, 1, 2);
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	float xscale = luaL_checknumber(L, 2);
	float yscale = luaL_optnumber(L, 3, xscale);
	gfx_matrix_scale2d(&win->modelview, xscale, yscale);
	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: rotate
 * Synopsis: window:rotate(angle)
 */
static int gfx_rotate(lua_State *L)
{
	gfx_check_nargs(1, 1, 1);
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	float angle = luaL_checknumber(L, 2);
	gfx_matrix_rotate2d(&win->modelview, angle);
	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: translate
 * Synopsis: window:translate(xoffs, yoffs = 0)
 */
static int gfx_translate(lua_State *L)
{
	gfx_check_nargs(1, 1, 2);
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	float xoffs = luaL_checknumber(L, 2);
	float yoffs = luaL_optnumber(L, 3, 0);
	gfx_matrix_translate2d(&win->modelview, xoffs, yoffs);
	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: settransform
 * Synopsis: window:settransform(transform = nil)
 */
static int gfx_settransform(lua_State *L)
{
	gfx_check_nargs(1, 0, 1);
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	if (lua_gettop(L) > 1) {
		gfx_matrix *m = gfx_checkMatrix(L, 2);
		memcpy(&win->modelview, m, sizeof(gfx_matrix));
	} else {
		gfx_matrix_loadidentity(&win->modelview);
	}
	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: gettransform
 * Synopsis: transform = window:gettransform()
 */
static int gfx_gettransform(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	gfx_matrix *m = gfx_pushMatrix(L);
	memcpy(m, &win->modelview, sizeof(gfx_matrix));
	return 1;
}

static int gfx_setupForDrawing(lua_State *L, int idx, int usetex)
{
	gfx_window *win = (gfx_window*) lua_touserdata(L, idx);
	GLuint shid = gfx_getShaderId(L, idx);
	glUseProgram(shid);
	gfx_shaderSetUniformMatrix(shid, GFX_MATRIX_PROJECTION, &win->projection);
	gfx_shaderSetUniformMatrix(shid, GFX_MATRIX_MODELVIEW, &win->modelview);
	gfx_shaderSetUniformColor(shid, "fgcolor", win->fgcolor);
	gfx_shaderSetUniformColor(shid, "bgcolor", win->bgcolor);
	if (usetex) {
		gfx_setupTextures(L, idx);
	}
	return 1;
}

int gfx_drawstuff(lua_State *L, int idx, GLenum mode, int ncoords, gfx_vec2 *coords, gfx_vec2 *texcs, gfx_color *cols)
{
	gfx_window *win = gfx_checkAndUseWindow(L, idx);
	if (!gfx_setupForDrawing(L, idx, texcs != 0)) {
		return 0;
	}
	GLuint shid = gfx_getShaderId(L, idx);

	glBindVertexArray(win->vao);
	glBindBuffer(GL_ARRAY_BUFFER, win->vbo);
	int blen = ncoords * sizeof(gfx_vec2);
	if (texcs != 0) { blen += ncoords * sizeof(gfx_vec2); }
	if (cols != 0) { blen += ncoords * sizeof(gfx_color); }
	if (blen > win->vbosize) {
		glBufferData(GL_ARRAY_BUFFER, blen, NULL, GL_STATIC_DRAW);
		win->vbosize = blen;
	}

	long ofs = 0;
	
	if (coords != 0) {
		glBufferSubData(GL_ARRAY_BUFFER, ofs, ncoords * sizeof(gfx_vec2), coords);
		glVertexAttribPointer(GFX_POS_COORDS, 2, GL_FLOAT, GL_FALSE, 0, (void*)ofs);
		glEnableVertexAttribArray(GFX_POS_COORDS);
		ofs += ncoords * sizeof(gfx_vec2);
	}

	if (texcs != 0) {
		glBufferSubData(GL_ARRAY_BUFFER, ofs, ncoords * sizeof(gfx_vec2), texcs);
		glVertexAttribPointer(GFX_POS_TEXCS, 2, GL_FLOAT, GL_FALSE, 0, (void*)ofs);
		glEnableVertexAttribArray(GFX_POS_TEXCS);
		gfx_shaderSetUniformInt(shid, "usetexc", 1);
		ofs += ncoords * sizeof(gfx_vec2);
	} else {
		gfx_shaderSetUniformInt(shid, "usetexc", 0);
	}

	if (cols != 0) {
		glBufferSubData(GL_ARRAY_BUFFER, ofs, ncoords * sizeof(gfx_color), cols);
		glVertexAttribPointer(GFX_POS_COLS, 4, GL_FLOAT, GL_FALSE, 0, (void*)ofs);
		glEnableVertexAttribArray(GFX_POS_COLS);
		gfx_shaderSetUniformInt(shid, "usecols", 1);
	} else {
		gfx_shaderSetUniformInt(shid, "usecols", 0);
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(mode, 0, ncoords);

	glDisableVertexAttribArray(GFX_POS_COLS);
	glDisableVertexAttribArray(GFX_POS_TEXCS);
	glDisableVertexAttribArray(GFX_POS_COORDS);
	glBindVertexArray(0);
	glUseProgram(0);

	if (win->mode == DIRECT) {
		glFlush();
	}

	return 0;
}

static void gfx_vec2TableToArray(lua_State *L, int idx, gfx_vec2 *coords, int len, int which)
{
	int cidx = 0;
	for (int n = 1; n <= len; n += 2) {
		int type = lua_rawgeti(L, idx, n);
		if (type == LUA_TNUMBER) {
			type = lua_rawgeti(L, idx, n + 1);
		}
		if (type == LUA_TNUMBER) {
			coords[cidx].x = lua_tonumber(L, -2);
			coords[cidx].y = lua_tonumber(L, -1);
			lua_pop(L, 2);
		} else {
			luaL_argerror(L, which, "all entries in table must be numbers");
		}
		++cidx;
	}
}

static void gfx_colorTableToArray(lua_State *L, int idx, gfx_color *cols, int len, int which)
{
	int cidx = 0;
	for (int n = 0; n < len; n += 1) {
		if (lua_rawgeti(L, idx, n * 4 + 1) != LUA_TNUMBER ||
			lua_rawgeti(L, idx, n * 4 + 2) != LUA_TNUMBER ||
			lua_rawgeti(L, idx, n * 4 + 3) != LUA_TNUMBER ||
			lua_rawgeti(L, idx, n * 4 + 4) != LUA_TNUMBER) {
			luaL_argerror(L, which, "all entries in table must be numbers");
		}
		cols[cidx].r = lua_tonumber(L, -4);
		cols[cidx].g = lua_tonumber(L, -3);
		cols[cidx].b = lua_tonumber(L, -2);
		cols[cidx].a = lua_tonumber(L, -1);
		lua_pop(L, 4);
		++cidx;
	}
}

/*** Method
 * Object: window
 * Name: line
 * Synopsis: window:line(x1, y1, x2, y2)
 */
static int gfx_line(lua_State *L)
{
	gfx_check_nargs(1, 4, 4);
	(void) gfx_checkAndUseWindow(L, 1);
	gfx_vec2 coords[2];
	coords[0].x = luaL_checknumber(L, 2);
	coords[0].y = luaL_checknumber(L, 3);
	coords[1].x = luaL_checknumber(L, 4);
	coords[1].y = luaL_checknumber(L, 5);

	gfx_drawstuff(L, 1, GL_LINES, 2, coords, 0, 0);

	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: lines
 * Synopsis: window:lines({coords}, type = "lines")
 * type = "lines" || "strip" || "loop"
 */
static int gfx_lines(lua_State *L)
{
	gfx_check_nargs(1, 1, 2);
	(void) gfx_checkAndUseWindow(L, 1);
	luaL_checktype(L, 2, LUA_TTABLE);
	int type = GL_LINES;
	if (lua_gettop(L) > 2) {
		const char *tname = lua_tostring(L, 3);
		if (strcmp(tname, "lines") == 0) {
			type = GL_LINES;
		} else if (strcmp(tname, "strip") == 0) {
			type = GL_LINE_STRIP;
		} else if (strcmp(tname, "loop") == 0) {
			type = GL_LINE_LOOP;
		} else {
			return luaL_argerror(L, 1, "unknown type");
		}
	}

	int len = gfx_tableLen(L, 2);

	if (type == GL_LINES && (len % 4) != 0) {
		return luaL_argerror(L, 2, "table length must be a multiple of 4 for type 'lines'");
	}
	if (type != GL_LINES && (len & 1) != 0) {
		return luaL_argerror(L, 2, "table length must be even");
	}

	int alen = len >> 1;
	gfx_vec2 coords[alen];
	gfx_vec2TableToArray(L, 2, coords, len, 2);

	gfx_drawstuff(L, 1, type, alen, coords, 0, 0);

	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: point
 * Synopsis: window:point(x, y)
 */
static int gfx_point(lua_State *L)
{
	gfx_check_nargs(1, 2, 2);
	(void) gfx_checkAndUseWindow(L, 1);
	gfx_vec2 coords;
	coords.x = luaL_checkinteger(L, 2);
	coords.y = luaL_checkinteger(L, 3);

	gfx_drawstuff(L, 1, GL_POINTS, 1, &coords, 0, 0);

	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: points
 * Synopsis: window:points({coords})
 */
static int gfx_points(lua_State *L)
{
	gfx_check_nargs(1, 1, 1);
	(void) gfx_checkAndUseWindow(L, 1);
	luaL_checktype(L, 2, LUA_TTABLE);
	int len = gfx_tableLen(L, 2);

	if ((len & 1) != 0) {
		return luaL_argerror(L, 2, "table length must be even");
	}

	int alen = len >> 1;
	gfx_vec2 coords[alen];
	gfx_vec2TableToArray(L, 2, coords, len, 2);

	gfx_drawstuff(L, 1, GL_POINTS, alen, coords, 0, 0);

	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: rectangle
 * Synopsis: window:rectangle(x, y, w, h, fill = true)
 */
static int gfx_rectangle(lua_State *L)
{
	gfx_check_nargs(1, 4, 5);
	(void) gfx_checkAndUseWindow(L, 1);
	gfx_rect r;
	int fill = 1;
	r.x = luaL_checkinteger(L, 2);
	r.y = luaL_checkinteger(L, 3);
	r.w = luaL_checkinteger(L, 4);
	r.h = luaL_checkinteger(L, 5);
	if (lua_gettop(L) > 5) {
		fill = lua_toboolean(L, 6);
	}

	gfx_vec2 coords[4];
	coords[0].x = r.x;
	coords[0].y = r.y;
	coords[1].x = r.x + r.w - 1;
	coords[1].y = r.y;
	if (fill) {
		coords[2].x = r.x;
		coords[2].y = r.y + r.h - 1;
		coords[3].x = r.x + r.w - 1;
		coords[3].y = r.y + r.h - 1;
		gfx_drawstuff(L, 1, GL_TRIANGLE_STRIP, 4, coords, 0, 0);
	} else {
		coords[2].x = r.x + r.w - 1;
		coords[2].y = r.y + r.h - 1;
		coords[3].x = r.x;
		coords[3].y = r.y + r.h - 1;
		gfx_drawstuff(L, 1, GL_LINE_LOOP, 4, coords, 0, 0);
	}

	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: triangle
 * Synopsis: window:triangle(x1, y1, x2, y2, x3, y3, fill = true)
 */
static int gfx_triangle(lua_State *L)
{
	gfx_check_nargs(1, 6, 7);
	(void) gfx_checkAndUseWindow(L, 1);
	gfx_vec2 coords[3];
	
	coords[0].x = luaL_checknumber(L, 2);
	coords[0].y = luaL_checknumber(L, 3);
	coords[1].x = luaL_checknumber(L, 4);
	coords[1].y = luaL_checknumber(L, 5);
	coords[2].x = luaL_checknumber(L, 6);
	coords[2].y = luaL_checknumber(L, 7);

	if (lua_gettop(L) == 7 || lua_toboolean(L, 8) == 1) {
		gfx_drawstuff(L, 1, GL_TRIANGLES, 3, coords, 0, 0);
	} else {
		gfx_drawstuff(L, 1, GL_LINE_LOOP, 3, coords, 0, 0);
	}

	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: draw
 * Synopsis: window:draw(type, {coords}, {texc} = nil, {cols} = nil)
 * type = "points", "line_strip", "line_loop", "lines", "triangle_strip", "triangle_fan", "triangles"
 */
static int gfx_draw(lua_State *L)
{
	gfx_check_nargs(1, 2, 4);
	(void) gfx_checkAndUseWindow(L, 1);
	const char *tname = luaL_checkstring(L, 2);
	luaL_checktype(L, 3, LUA_TTABLE);
	int has_texc = 0, has_cols = 0;
	if (lua_gettop(L) > 3 && !lua_isnil(L, 4)) {
		luaL_checktype(L, 4, LUA_TTABLE);
		has_texc = 1;
	}
	if (lua_gettop(L) > 4 && lua_isnil(L, 5)) {
		luaL_checktype(L, 5, LUA_TTABLE);
		has_cols = 1;
	}

	int type = 0, mult_of = 2;

	if (strcmp(tname, "points") == 0) {
		type = GL_POINTS;
	} else if (strcmp(tname, "line_strip") == 0) {
		type = GL_LINE_STRIP;
	} else if (strcmp(tname, "line_loop") == 0) {
		type = GL_LINE_LOOP;
	} else if (strcmp(tname, "lines") == 0) {
		type = GL_LINES;
		mult_of = 4;
	} else if (strcmp(tname, "triangle_strip") == 0) {
		type = GL_TRIANGLE_STRIP;
	} else if (strcmp(tname, "triangle_fan") == 0) {
		type = GL_TRIANGLE_FAN;
	} else if (strcmp(tname, "triangles") == 0) {
		type = GL_TRIANGLES;
		mult_of = 6;
	} else {
		return luaL_argerror(L, 2, "unknown type");
	}

	int len = gfx_tableLen(L, 3);
	if (len % mult_of != 0) {
		const char *errfmt = "table length must be a multiple of %d";
		int blen = snprintf(0, 0, errfmt, mult_of);
		char buf[blen+1];
		snprintf(buf, blen+1, errfmt, mult_of);
		return luaL_argerror(L, 3, buf);
	}
	if (has_texc && len != gfx_tableLen(L, 4)) {
		return luaL_argerror(L, 4, "table has wrong length");
	}
	if (has_cols && (len * 2 != gfx_tableLen(L, 5))) {
		return luaL_argerror(L, 5, "table has wrong length");
	}

	int alen = len >> 1;
	gfx_vec2 coords[alen];
	gfx_vec2 texc[has_texc ? alen : 0];
	gfx_color cols[has_cols ? alen : 0];

	gfx_vec2TableToArray(L, 3, coords, len, 3);
	if (has_texc) {
		gfx_vec2TableToArray(L, 4, texc, len, 4);
	}
	if (has_cols) {
		gfx_colorTableToArray(L, 5, cols, alen, 5);
	}

	gfx_drawstuff(L, 1, type, alen, coords, has_texc ? texc : 0, has_cols ? cols : 0);

	lua_pushvalue(L, 1);
	return 1;
}

static const luaL_Reg gfx_window_draw_methods[] = {
	{"clear", gfx_clear},
	{"scale", gfx_scale},
	{"rotate", gfx_rotate},
	{"translate", gfx_translate},
	{"settransform", gfx_settransform},
	{"gettransform", gfx_gettransform},
	{"setfg", gfx_setfg},
	{"getfg", gfx_getfg},
	{"setbg", gfx_setbg},
	{"getbg", gfx_getbg},
	{"line", gfx_line},
	{"lines", gfx_lines},
	{"point", gfx_point},
	{"points", gfx_points},
	{"rectangle", gfx_rectangle},
	{"triangle", gfx_triangle},
	{"draw", gfx_draw},
	{0, 0}
};

/*** Function
 * Name: gentexturedrect
 * Synopsis: {coords}, {texcs} = gfx.gentexturedrect([cx, cy, cw, ch, [tx, ty, tw, th]])
 * rect = { x=..., y=..., w=..., h=... }
 */
static int gfx_gentexturedrect(lua_State *L)
{
	gfx_check_nargs(0, 4, 8);
	gfx_rect coord_r = {0, 0, 1, 1}, texc_r = {0, 0, 1, 1};
	if (lua_gettop(L) > 0) {
		coord_r.x = luaL_checknumber(L, 1);
		coord_r.y = luaL_checknumber(L, 2);
		coord_r.w = luaL_checknumber(L, 3);
		coord_r.h = luaL_checknumber(L, 4);
	}

	if (lua_gettop(L) > 4) {
		texc_r.x = luaL_checknumber(L, 5);
		texc_r.y = luaL_checknumber(L, 6);
		texc_r.w = luaL_checknumber(L, 7);
		texc_r.h = luaL_checknumber(L, 8);
	}

	gfx_vec2 coords[6], texcs[6];
	gfx_makeTexturedRectangle(&coord_r, &texc_r, coords, texcs);
	lua_newtable(L);
	int cpos = lua_gettop(L);
	lua_newtable(L);
	int tpos = lua_gettop(L);
	for (int i = 0; i < 6; ++i) {
		lua_pushnumber(L, coords[i].x);
		lua_rawseti(L, cpos, i * 2 + 1);
		lua_pushnumber(L, coords[i].y);
		lua_rawseti(L, cpos, i * 2 + 2);

		lua_pushnumber(L, texcs[i].x);
		lua_rawseti(L, tpos, i * 2 + 1);
		lua_pushnumber(L, texcs[i].y);
		lua_rawseti(L, tpos, i * 2 + 2);
	}
	return 2;
}

static const luaL_Reg gfx_draw_funcs[] = {
	{"gentexturedrect", gfx_gentexturedrect},
	{0, 0}
};

int gfx_registerDrawFunctions(lua_State *L)
{
	gfx_createHelperObject(L, GFX_MATRIX);

	luaL_setfuncs(L, gfx_draw_funcs, 0);
	return 1;
}
int gfx_registerDrawMethods(lua_State *L)
{
	luaL_setfuncs(L, gfx_window_draw_methods, 0);
	return 1;
}
