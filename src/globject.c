/* globject.c
 *
 * common lua wrapper for opengl resource objects
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"

gfx_globject* gfx_pushGLObject(lua_State *L, const char *objtype, gfx_globject_destructor destroy)
{
	gfx_globject *obj = (gfx_globject*) lua_newuserdata(L, sizeof(gfx_globject));
	memset(obj, 0, sizeof(gfx_globject));
	obj->destroy = destroy;
	luaL_getmetatable(L, objtype);
	lua_setmetatable(L, -2);
	return obj;
}

static int gfx_globject__gc(lua_State *L)
{
	gfx_globject *obj = lua_touserdata(L, 1);
	if (obj->destroy && obj->id) {
		obj->destroy(obj->id);
	}
	obj->id = 0;
	return 0;
}

static int gfx_globject__toString(lua_State *L)
{
	void *obj = lua_touserdata(L, 1);
	const char *type = "(globject)";
	if (luaL_getmetafield (L, 1, "__name") != LUA_TNIL) {
		type = lua_tostring(L, -1);
		lua_pop(L, 1);
	}
	lua_pushfstring(L, "%s: %p", type, obj);
	return 1;
}

/* metamethods for the globject userdata
 */
static const luaL_Reg gfx_globject_meta[] = {
	{"__gc", gfx_globject__gc},
	{"__tostring", gfx_globject__toString},
	{0, 0}
};

int gfx_newGLObject(lua_State *L, const char *objtype, const luaL_Reg *methods)
{
	luaL_newmetatable(L, objtype);
	luaL_setfuncs(L, gfx_globject_meta, 0);
	lua_pushliteral(L, "__index");
	lua_newtable(L);
	luaL_setfuncs(L, methods, 0);
	lua_rawset(L, -3);
	lua_pop(L, 1);
	return 0;
}
