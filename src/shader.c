/* shader.c
 *
 * shaders for gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */
#include "gfx.h"

#include <ctype.h>

/*** Module
 * Name: gfx
 */

static const char *vertex_shader = GLSL(330,
	layout (location=0) in vec4 pos_in;
	layout (location=1) in vec2 texc_in;
	layout (location=2) in vec4 col_in;
	uniform mat4 projection_matrix;
	uniform mat4 modelview_matrix;
	out vec2 itexc;
	out vec4 icolor;
	void main() {
		gl_Position = projection_matrix * modelview_matrix * pos_in;
		itexc = texc_in;
		icolor = col_in;
	}
);

static const char *fragment_shader = GLSL(150,
	in vec2 itexc;
	in vec4 icolor;
	uniform bool usetexc;
	uniform bool usecols;
	uniform vec4 fgcolor;
	uniform vec4 bgcolor;
	uniform sampler2D texture0;
	out vec4 fragColor;
	void main() {
		fragColor = fgcolor;
		if (usetexc) {
			fragColor *= texture(texture0, itexc);
		}
		if (usecols) {
			fragColor *= icolor;
		}
	}
);

static int gfx_shaderCompile(GLuint shader, const char *src)
{
	GLint compiled;
	glShaderSource(shader, 1, &src, 0);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	return compiled == GL_TRUE;
}

static int mystrtok(const char *s, const char *delim, int pos, int *start, int *end)
{
	int c, i = pos;
	while ((c = s[i]) && strchr(delim, c)) {
		i += 1;
	}
	if (!c) {
		return 0;
	}
	*start = i;
	i += 1;
	while ((c = s[i]) && (strchr(delim, c) == NULL)) {
		i += 1;
	}
	*end = i - 1;
	return 1;
}

static int gfx_shaderHasVersion(const char *s)
{
	int start = 0, end = 0;
	if (mystrtok(s, " \t\r\n", 0, &start, &end) == 0) {
		return 0;
	}
	if (strncmp("#version", s + start, (end - start + 1)) != 0) {
		return 0;
	}
	return 1;
}

GLuint gfx_shaderBuild(lua_State *L, const char *frag_shader)
{
	GLuint vertsh = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragsh = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint shader;

	int ok = gfx_shaderCompile(shader = vertsh, vertex_shader);
	int hasver = gfx_shaderHasVersion(frag_shader);
	if (!hasver) {
		lua_pushfstring(L, GFX_SHADER_DEFAULT_VERSION "\n%s", frag_shader);
		frag_shader = lua_tostring(L, -1);
	}
	ok = ok && gfx_shaderCompile(shader = fragsh, frag_shader);
	if (!hasver) {
		lua_pop(L, 1);
	}

	if (!ok) {
		GLint log_length = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH , &log_length);
		if (log_length > 1) {
			char msg[log_length];
			glGetShaderInfoLog(shader, log_length, 0, msg);
			glDeleteShader(vertsh);
			glDeleteShader(fragsh);
			return luaL_error(L, "could not compile shader: %s", msg);
		}
	}

	GLuint id = glCreateProgram();
	glAttachShader(id, vertsh);
	glAttachShader(id, fragsh);
	glLinkProgram(id);
	glGetProgramiv(id, GL_LINK_STATUS, &ok);

	if (ok != GL_TRUE) {
		glDeleteShader(vertsh);
		glDeleteShader(fragsh);
		GLsizei log_length = 0;
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &log_length);
		char msg[log_length];
		glGetProgramInfoLog(id, log_length, 0, msg);
		glDeleteProgram(id);
		return luaL_error(L, "could not link shader: %s", msg);
	}

	glDeleteShader(vertsh);
	glDeleteShader(fragsh);
	return id;
}

static GLuint gfx_shaderGetDefault(lua_State *L)
{
	static GLuint default_shader = 0;
	if (default_shader == 0) {
		default_shader = gfx_shaderBuild(L, fragment_shader);
	}
	return default_shader;
}

void gfx_shaderSetUniformMatrix(GLuint id, const char *which, gfx_matrix *m)
{
	int loc =glGetUniformLocation(id, which);
	if (loc > -1) {
		glUniformMatrix4fv(loc, 1, GL_FALSE, *m);
	}
}

void gfx_shaderSetUniformInt(GLuint id, const char *name, int val)
{
	int loc = glGetUniformLocation(id, name);
	if (loc > -1) {
		glUniform1i(loc, val);
	}
}

void gfx_shaderSetUniformVec2(GLuint id, const char *name, gfx_vec2 v2)
{
	int loc = glGetUniformLocation(id, name);
	if (loc > -1) {
		glUniform2f(loc, v2.x, v2.y);
	}
}

void gfx_shaderSetUniformColor(GLuint id, const char *name, gfx_color col)
{
	int loc = glGetUniformLocation(id, name);
	if (loc > -1) {
		glUniform4f(loc, col.r, col.g, col.b, col.a);
	}
}

/*** Object
 * Name: shader
 */

/* shader object methods */

/*** Method
 * Object: shader
 * Name: setint
 * Synopsis: ok = setint(name, i1 [, i2 [, i3 [, i4]]])
 */
static int gfx_shader_setint(lua_State *L)
{
	gfx_shader *sh = gfx_checkShader(L, 1);
	const char *name = luaL_checkstring(L, 2);
	GLint loc = glGetUniformLocation(sh->id, name);
	if (loc == -1) {
		lua_pushboolean(L, 0);
		return 1;
	}
	int top = lua_gettop(L);
	GLint i1 = luaL_checkinteger(L, 3);
	if (top > 3) {
		GLint i2 = luaL_checkinteger(L, 4);
		if (top > 4) {
			GLint i3 = luaL_checkinteger(L, 5);
			if (top > 5) {
				GLint i4 = luaL_checkinteger(L, 6);
				glUniform4i(loc, i1, i2, i3, i4);
			} else {
				glUniform3i(loc, i1, i2, i3);
			}
		} else {
			glUniform2i(loc, i1, i2);
		}
	} else {
		glUniform1i(loc, i1);
	}
	lua_pushboolean(L, 1);
	return 1;
}

/*** Method
 * Object: shader
 * Name: setintv
 * Synopsis: ok = setintv(name, {...}, size = 1)
 */
static int gfx_shader_setintv(lua_State *L)
{
	gfx_shader *sh = gfx_checkShader(L, 1);
	const char *name = luaL_checkstring(L, 2);
	GLint loc = glGetUniformLocation(sh->id, name);
	if (loc == -1) {
		lua_pushboolean(L, 0);
		return 1;
	}
	luaL_checktype(L, 3, LUA_TTABLE);
	int len = gfx_tableLen(L, 3);
	int size = luaL_optinteger(L, 4, 1);
	if (1 > size || size > 4) {
		return luaL_argerror(L, 4, "invalid size");
	}
	if (len % size != 0) {
		return luaL_argerror(L, 3, "invalid length");
	}
	GLint intv[len];
	for (int i = 0; i < len; ++i) {
		if (lua_rawgeti(L, 3, i + 1) != LUA_TNUMBER) {
			luaL_argerror(L, 3, "all entries in table must be numbers");
		}
		intv[i] = lua_tointeger(L, -1);
	}
	switch (size) {
		case 1:
			glUniform1iv(loc, len, intv);
			break;
		case 2:
			glUniform2iv(loc, len / size, intv);
			break;
		case 3:
			glUniform3iv(loc, len / size, intv);
			break;
		case 4:
			glUniform3iv(loc, len / size, intv);
			break;
		default:;
	}
	lua_pushboolean(L, 1);
	return 1;
}

/*** Method
 * Object: shader
 * Name: setuint
 * Synopsis: ok = setuint(name, u1 [, u2 [, u3 [, u4]]])
 */
static int gfx_shader_setuint(lua_State *L)
{
	gfx_shader *sh = gfx_checkShader(L, 1);
	const char *name = luaL_checkstring(L, 2);
	GLint loc = glGetUniformLocation(sh->id, name);
	if (loc == -1) {
		lua_pushboolean(L, 0);
		return 1;
	}
	int top = lua_gettop(L);
	GLuint i1 = luaL_checkinteger(L, 3);
	if (top > 3) {
		GLuint i2 = luaL_checkinteger(L, 4);
		if (top > 4) {
			GLuint i3 = luaL_checkinteger(L, 5);
			if (top > 5) {
				GLuint i4 = luaL_checkinteger(L, 6);
				glUniform4ui(loc, i1, i2, i3, i4);
			} else {
				glUniform3ui(loc, i1, i2, i3);
			}
		} else {
			glUniform2ui(loc, i1, i2);
		}
	} else {
		glUniform1ui(loc, i1);
	}
	lua_pushboolean(L, 1);
	return 1;
}

/*** Method
 * Object: shader
 * Name: setuintv
 * Synopsis: ok = setuintv(name, {...}, size)
 */
static int gfx_shader_setuintv(lua_State *L)
{
	gfx_shader *sh = gfx_checkShader(L, 1);
	const char *name = luaL_checkstring(L, 2);
	GLint loc = glGetUniformLocation(sh->id, name);
	if (loc == -1) {
		lua_pushboolean(L, 0);
		return 1;
	}
	luaL_checktype(L, 3, LUA_TTABLE);
	int len = gfx_tableLen(L, 3);
	int size = luaL_optinteger(L, 4, 1);
	if (1 > size || size > 4) {
		return luaL_argerror(L, 4, "invalid size");
	}
	if (len % size != 0) {
		return luaL_argerror(L, 3, "invalid length");
	}
	GLuint uintv[len];
	for (int i = 0; i < len; ++i) {
		if (lua_rawgeti(L, 3, i + 1) != LUA_TNUMBER) {
			luaL_argerror(L, 3, "all entries in table must be numbers");
		}
		uintv[i] = lua_tointeger(L, -1);
	}
	switch (size) {
		case 1:
			glUniform1uiv(loc, len, uintv);
			break;
		case 2:
			glUniform2uiv(loc, len / size, uintv);
			break;
		case 3:
			glUniform3uiv(loc, len / size, uintv);
			break;
		case 4:
			glUniform3uiv(loc, len / size, uintv);
			break;
		default:;
	}
	lua_pushboolean(L, 1);
	return 1;
}

/*** Method
 * Object: shader
 * Name: setfloat
 * Synopsis: ok = setfloat(name, f1 [, f2 [, f3 [, f4]]])
 */
static int gfx_shader_setfloat(lua_State *L)
{
	gfx_shader *sh = gfx_checkShader(L, 1);
	const char *name = luaL_checkstring(L, 2);
	GLint loc = glGetUniformLocation(sh->id, name);
	if (loc == -1) {
		lua_pushboolean(L, 0);
		return 1;
	}
	int top = lua_gettop(L);
	GLint f1 = luaL_checknumber(L, 3);
	if (top > 3) {
		GLint f2 = luaL_checknumber(L, 4);
		if (top > 4) {
			GLint f3 = luaL_checknumber(L, 5);
			if (top > 5) {
				GLint f4 = luaL_checknumber(L, 6);
				glUniform4f(loc, f1, f2, f3, f4);
			} else {
				glUniform3f(loc, f1, f2, f3);
			}
		} else {
			glUniform2f(loc, f1, f2);
		}
	} else {
		glUniform1f(loc, f1);
	}
	lua_pushboolean(L, 1);
	return 1;
}

/*** Method
 * Object: shader
 * Name: setfloatv
 * Synopsis: ok = setfloatv(name, {...}, size)
 */
static int gfx_shader_setfloatv(lua_State *L)
{
	gfx_shader *sh = gfx_checkShader(L, 1);
	const char *name = luaL_checkstring(L, 2);
	GLint loc = glGetUniformLocation(sh->id, name);
	if (loc == -1) {
		lua_pushboolean(L, 0);
		return 1;
	}
	luaL_checktype(L, 3, LUA_TTABLE);
	int len = gfx_tableLen(L, 3);
	int size = luaL_optinteger(L, 4, 1);
	if (1 > size || size > 4) {
		return luaL_argerror(L, 4, "invalid size");
	}
	if (len % size != 0) {
		return luaL_argerror(L, 3, "invalid length");
	}
	GLfloat fltv[len];
	for (int i = 0; i < len; ++i) {
		if (lua_rawgeti(L, 3, i + 1) != LUA_TNUMBER) {
			luaL_argerror(L, 3, "all entries in table must be numbers");
		}
		fltv[i] = lua_tonumber(L, -1);
	}
	switch (size) {
		case 1:
			glUniform1fv(loc, len, fltv);
			break;
		case 2:
			glUniform2fv(loc, len / size, fltv);
			break;
		case 3:
			glUniform3fv(loc, len / size, fltv);
			break;
		case 4:
			glUniform3fv(loc, len / size, fltv);
			break;
		default:;
	}
	lua_pushboolean(L, 1);
	return 1;
}

static const luaL_Reg gfx_shader_methods[] = {
	{"setint", gfx_shader_setint},
	{"setintv", gfx_shader_setintv},
	{"setuint", gfx_shader_setuint},
	{"setuintv", gfx_shader_setuintv},
	{"setfloat", gfx_shader_setfloat},
	{"setfloatv", gfx_shader_setfloatv},
	{0, 0}
};

/* functions */

/*** Constructor
 * Object: shader
 * Name: shader
 * Synopsis: shader = gfx.shader(source)
 */
static int gfx_newshader(lua_State *L)
{
	const char *src = luaL_checkstring(L, 1);
	gfx_shader *s = gfx_pushShader(L);
	s->id = gfx_shaderBuild(L, src);
	return 1;
}

static const luaL_Reg gfx_shader_funcs[] = {
	{"shader", gfx_newshader},
	{0, 0}
};

/* window methods */

/*** Method
 * Object: window
 * name: setshader
 * Synopsis: window:setshader(shader = nil)
 */
static int gfx_window_setshader(lua_State *L)
{
	gfx_checkAndUseWindow(L, 1);
	if (!lua_isnoneornil(L, 2)) {
		gfx_checkShader(L, 2);
	} else {
		if (lua_gettop(L) > 1) {
			lua_pop(L, lua_gettop(L) - 1);
		}
		gfx_shader *s = gfx_pushShader(L);
		s->destroy = NULL; // don't destroy default shaders!
		s->id = gfx_shaderGetDefault(L);
	}
	gfx_setProperty(L, 1, 2, WINDOW_PROP_SHADER);
	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * name: getshader
 * Synopsis: shader = window:getshader()
 */
static int gfx_window_getshader(lua_State *L)
{
	gfx_checkAndUseWindow(L, 1);
	gfx_getProperty(L, 1, WINDOW_PROP_SHADER);
	return 1;
}

GLuint gfx_getShaderId(lua_State *L, int idx)
{
	gfx_checkAndUseWindow(L, idx);
	if (gfx_getProperty(L, 1, WINDOW_PROP_SHADER) != LUA_TNIL) {
		gfx_shader *sh = gfx_checkShader(L, -1);
		lua_pop(L, 1);
		return sh->id;
	}
	lua_pop(L, 1);
	return 0;
}

static const luaL_Reg gfx_window_shader_methods[] = {
	{"setshader", gfx_window_setshader},
	{"getshader", gfx_window_getshader},
	{0, 0}
};


int gfx_registerShaderFunctions(lua_State *L)
{
	gfx_newGLObject(L, GFX_SHADER, gfx_shader_methods);

	/* register functions */
	luaL_setfuncs(L, gfx_shader_funcs, 0);
	return 1;
}

int gfx_registerShaderMethods(lua_State *L)
{
	luaL_setfuncs(L, gfx_window_shader_methods, 0);
	return 1;
}
