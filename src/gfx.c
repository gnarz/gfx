/* gfx.c
 *
 * utilities and simple global functions for gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"

/*** Module
 * Name: gfx
 */

/* module finalizers */

static struct gfx_finalizer_chain {
	gfx_finalizer finalizer;
	struct gfx_finalizer_chain *next;
} *gfx_finalizers = NULL;

void gfx_registerFinalizer(gfx_finalizer f)
{
	struct gfx_finalizer_chain *entry = malloc(sizeof(struct gfx_finalizer_chain));
	assert(entry != NULL);
	entry->finalizer = f;
	entry->next = 0;
	if (gfx_finalizers) {
		struct gfx_finalizer_chain *e = gfx_finalizers;
		while (e->next) {
			e = e->next;
		}
		e->next = entry;
	} else {
		gfx_finalizers = entry;
	}
}

static void gfx_runAndCleanupFinalizers(lua_State *L)
{
	struct gfx_finalizer_chain *e = gfx_finalizers, *n = NULL;
	while (e) {
		n = e->next;
		e->finalizer(L);
		free(e);
		e = n;
	}
	gfx_finalizers = NULL;
}

/* utility stuff */

int gfx_tableLen(lua_State *L, int idx)
{
	lua_len(L, idx);
	int res = lua_tointeger(L, -1);
	lua_pop(L, 1);
	return res;
}

int gfx_sdlError(lua_State *L, const char *msg)
{
	const char *errstr = SDL_GetError();
	if (errstr && *errstr) {
		SDL_ClearError();
		if (msg) {
			return luaL_error(L, "%s: %s", msg, errstr);
		} else {
			return luaL_error(L, "%s", errstr);
		}
	}
	return 0;
}

int gfx_checkGLerror(lua_State *L, const char *msg)
{
	int err = glGetError();
	if (err != GL_NO_ERROR) {
		const char *errstr;
		switch (err) {
			/* case GL_NO_ERROR: errstr = "GL_NO_ERROR"; break; */
			case GL_INVALID_ENUM: errstr = "GL_INVALID_ENUM"; break;
			case GL_INVALID_VALUE: errstr = "GL_INVALID_VALUE"; break;
			case GL_INVALID_OPERATION: errstr = "GL_INVALID_OPERATION"; break;
			case GL_STACK_OVERFLOW: errstr = "GL_STACK_OVERFLOW"; break;
			case GL_STACK_UNDERFLOW: errstr = "GL_STACK_UNDERFLOW"; break;
			case GL_OUT_OF_MEMORY: errstr = "GL_OUT_OF_MEMORY"; break;
			case GL_TABLE_TOO_LARGE: errstr = "GL_TABLE_TOO_LARGE"; break;
			case GL_INVALID_FRAMEBUFFER_OPERATION: errstr = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
			default: errstr = "(unknown GL error)";
		}
		if (msg) {
			return luaL_error(L, "%s: %s", msg, errstr);
		} else {
			return luaL_error(L, "%s", errstr);
		}
		
	}
	return 0;
}

/* object properties (i.e. entries in an objects uservalue table) */
int gfx_setProperty(lua_State *L, int idx, int val, const char *what)
{
	if (val < 0) { val = lua_gettop(L) + 1 + val; }
	if (lua_getuservalue(L, idx) != LUA_TTABLE) {
		return luaL_error(L, "object has no property table.");
	}
	lua_pushvalue(L, val);
	lua_setfield(L, -2, what);
	lua_pop(L, 1);
	return 1;
}

int gfx_getProperty(lua_State *L, int idx, const char *what)
{
	if (lua_getuservalue(L, idx) != LUA_TTABLE) {
		return luaL_error(L, "object has no property table.");
	}
	int res = lua_getfield(L, -1, what);
	lua_replace(L, -2);
	return res;
}

/*** Function
 * Name: getms
 * Synopsis: ms = gfx.getms()
 * returns ms in milliseconds
 */
static int gfx_getms(lua_State *L)
{
	gfx_check_nargs(0, 0, 0);
	lua_pushinteger(L, SDL_GetTicks());
	return 1;
}

/*** Function
 * Name: waitms
 * Synopsis: gfx.waitms(ms)
 */
static int gfx_waitms(lua_State *L)
{
	gfx_check_nargs(0, 1, 1);
	uint32_t ms = luaL_checkinteger(L, 1);
	SDL_Delay(ms);
	return 0;
}

static int gfx_finalize(lua_State *L)
{
	gfx_runAndCleanupFinalizers(L);
	SDL_Quit();
	return 0;
}

/*** Function
 * Name: mainloop
 * Syopsis: gfx.mainloop()
 * TODO: proper name
 */
static int gfx_mainloop(lua_State *L)
{
	gfx_check_nargs(0, 0, 0);
	int quit = 0;
	do {
		if (gfx_pollEvent(L, 1, 0) > 0) {
			if (lua_getfield(L, -1, "type") != LUA_TNIL) {
				const char *type = lua_tostring(L, -1);
				lua_pop(L, 1);
				if (strcmp(type, "quit") == 0) {
					quit = 1;
				} else if (strcmp(type, "window") == 0) {
					if (lua_getfield(L, -1, "which") != LUA_TNIL) {
						const char *which = lua_tostring(L, -1);
						lua_pop(L, 1);
						if (strcmp(which, "close") == 0) {
							if (lua_getfield(L, -1, "window") != LUA_TNIL) {
								lua_getfield(L, -1, "close");
								lua_pushvalue(L, -2);
								lua_call(L, 1, 0);
								lua_pop(L, 1);
							}
						}
					}
				}
			}
			lua_pop(L, 1);
		}
	} while (!quit);
	return 1;
}

/*** Function
 * Name: exit
 * Synopsis: gfx.exit(status = 0)
 */
static int gfx_exit(lua_State *L)
{
	gfx_check_nargs(0, 0, 1);
	int status = luaL_optinteger(L, 1, 0);
	gfx_finalize(L);
	if (L) exit(status);
	return 0;
}

static const luaL_Reg gfx_funcs[] = {
	{"getms", gfx_getms},
	{"waitms", gfx_waitms},
	{"mainloop", gfx_mainloop},
	{"exit", gfx_exit},
	{0, 0}
};

int luaopen_gfx(lua_State *L)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		return gfx_sdlError(L, "can't initialize gfx");
	}

	lua_newtable(L);

	/* finalization handler */
	lua_newtable(L);
	lua_pushstring(L, "__gc");
	lua_pushcfunction(L, gfx_finalize);
	lua_settable(L, -3);
	lua_setmetatable(L, -2);

	/* register functions */
	luaL_setfuncs(L, gfx_funcs, 0);
	gfx_registerEventFunctions(L);
	gfx_registerTimerFunctions(L);
	gfx_registerWindowFunctions(L);
	gfx_registerShaderFunctions(L);
	gfx_registerDrawFunctions(L);
	gfx_registerImageFunctions(L);
	gfx_registerFontFunctions(L);

	return 1;
}
