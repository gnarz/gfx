/* window.c
 *
 * window handling for gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"

/*** Module
 * Name: gfx
 */

/* window registry */

static void gfx_setWindowRegistry(lua_State *L, uint32_t id, int pos)
{
	lua_getfield(L, LUA_REGISTRYINDEX, GFX_WINDOW_REGISTRY);
	lua_pushinteger(L, id);
	lua_pushvalue(L, pos);
	lua_settable(L, -3);
	lua_pop(L, 1);
}

static void gfx_registerWindow(lua_State *L, int pos)
{
	pos = pos > 0 ? pos : lua_gettop(L) + 1 + pos;
	gfx_window *win = (gfx_window*) lua_touserdata(L, pos);
	gfx_setWindowRegistry(L, win->id, pos);
}

static void gfx_unregisterWindow(lua_State *L, int pos)
{
	pos = pos > 0 ? pos : lua_gettop(L) + 1 + pos;
	gfx_window *win = (gfx_window*) lua_touserdata(L, pos);
	lua_pushnil(L);
	gfx_setWindowRegistry(L, win->id, pos+1);
	lua_pop(L, 1);
}

/* looks for a window in the window registry. If found, returns 1 and the
 * window on the stack, otherwise returns 0 and the stack unchanged.
 */
int gfx_findWindow(lua_State *L, uint32_t id)
{
	lua_pushstring(L, GFX_WINDOW_REGISTRY);
	lua_gettable(L, LUA_REGISTRYINDEX);
	lua_pushinteger(L, id);
	lua_gettable(L, -2);
	if (lua_isnoneornil(L, -1)) {
		lua_pop(L, 2);
		return 0;
	}
	lua_replace(L, -2);
	return 1;
}

static int gfx_allWindowsClosed(lua_State *L)
{
	lua_pushstring(L, GFX_WINDOW_REGISTRY);
	lua_gettable(L, LUA_REGISTRYINDEX);
	lua_pushnil(L);
	int empty = (lua_next(L, -2) == 0);
	lua_pop(L, empty ? 1 : 3);
	return empty;
}

static void gfx_finalizeWindowRegistry(lua_State *L)
{
	lua_pushstring(L, GFX_WINDOW_REGISTRY);
	lua_pushnil(L);
	lua_settable(L, LUA_REGISTRYINDEX);
}

/*** Object
 * Name: window
 * all window methods except for those starting with get return the window object they were called on.
 */

/* utils */
static void gfx_resizeWindow(gfx_window *win)
{
	int width, height;
	SDL_GL_GetDrawableSize(win->win, &width, &height);
	glViewport(0, 0, width, height);
	gfx_matrix_ortho(&win->projection, width, height);
}

gfx_window* gfx_checkWindow(lua_State *L, int index)
{
	gfx_window *win = (gfx_window*) luaL_checkudata(L, index, GFX_WINDOW);
	if (!win->win) {
		/* return */ luaL_error(L, "invalid window");
	}
	return win;
}

/* like gfx_checkWindow(), but if the check fails this returns 0 instead of
 * throwing an error.
 */
gfx_window* gfx_toWindow(lua_State *L, int index)
{
	gfx_window *win = (gfx_window*) lua_touserdata(L, index);
	if (lua_getmetatable(L, -1)) {
		luaL_getmetatable(L, GFX_WINDOW);
		int ok = lua_rawequal(L, -1, -2);
		lua_pop(L, 2);
		if (ok) {
			return win;
		}
	}
	return NULL;
}

static void gfx_setupFrameBuffer(lua_State *L, int idx)
{
	gfx_window *win = (gfx_window*) lua_touserdata(L, idx);
	gfx_image *img = NULL;
	if (gfx_getProperty(L, idx, WINDOW_PROP_FRAMEBUFFER) != LUA_TNIL) {
		img = gfx_checkImage(L, -1);
		lua_pop(L, 1);
	}
	lua_pop(L, 1);

	if (!img) {
		if (win->fbo) {
			glDeleteFramebuffers(1, &win->fbo);
			win->fbo = 0;
		}
		gfx_resizeWindow(win);
		glBindFramebuffer(GL_FRAMEBUFFER, win->fbo);
	} else {
		if (!win->fbo) {
			glGenFramebuffers(1, &win->fbo);
		}
		glBindFramebuffer(GL_FRAMEBUFFER, win->fbo);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, img->id, 0);
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE) {
			gfx_checkGLerror(L, "setup of framebuffer failed");
		}

		int width, height;
		gfx_imageSize(img, &width, &height, 0);
		glViewport(0, 0, width, height);
		gfx_matrix_ortho(&win->projection, width, height);
	}
}

/* gfx_checkAndUseWindow
 *
 * Checks whether the function argument narg is a userdata of the type
 * GFX_WINDOW. If so, makes it the current window and GL context, and
 * returns its block address, else throw an error.
 */
gfx_window* gfx_checkAndUseWindow(lua_State *L, int idx)
{
	static SDL_Window *current_win = 0;

	gfx_window *win = gfx_checkWindow(L, idx);
	if (win->win != current_win) {
		current_win = win->win;
		SDL_GL_MakeCurrent(win->win, win->ctx);
		gfx_setupFrameBuffer(L, idx);
	}

	return win;
}

static gfx_window* gfx_pushWindow(lua_State *L)
{
	gfx_window *win = (gfx_window*) lua_newuserdata(L, sizeof(gfx_window));
	memset(win, 0, sizeof(gfx_window));
	luaL_getmetatable(L, GFX_WINDOW);
	lua_setmetatable(L, -2);
	lua_newtable(L);
	lua_setuservalue(L, -2);
	return win;
}

static int gfx_window__gc(lua_State *L)
{
	gfx_window *win = (gfx_window*) lua_touserdata(L, 1);
	gfx_unregisterWindow(L, 1);
	if (win->fbo) { glDeleteFramebuffers(1, &win->fbo); }
	if (win->vbo) { glDeleteBuffers(1, &win->vbo); }
	if (win->vao) { glDeleteVertexArrays(1, &win->vao); }
	if (win->ctx) { SDL_GL_DeleteContext(win->ctx); }
	if (win->win) { SDL_DestroyWindow(win->win); }
	return 0;
}

static int gfx_window__toString(lua_State *L)
{
	void *win = (gfx_window*) lua_touserdata(L, 1);
	lua_pushfstring(L, "%s: %p", GFX_WINDOW, win);
	return 1;
}

/* metamethods for the window userdata
 */
static const luaL_Reg gfx_window_meta[] = {
	{"__gc", gfx_window__gc},
	{"__tostring", gfx_window__toString},
	{0, 0}
};

/* window methods */

/*** Method
 * Object: window
 * Name: hide
 * Synopsis: window:hide()
 */
static int gfx_window_hide(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_window *win = gfx_checkWindow(L, 1);
	SDL_HideWindow(win->win);
	return 0;
}

/*** Method
 * Object: window
 * Name: show
 * Synopsis: window:show()
 */
static int gfx_window_show(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_window *win = gfx_checkWindow(L, 1);
	SDL_HideWindow(win->win); // TODO seems to only work reliably if this is here... find out why
	SDL_ShowWindow(win->win);
	SDL_RaiseWindow(win->win);
	return 0;
}

/*** Method
 * Object: window
 * Name: close
 * Synopsis: window:close()
 * terminally closes window
 */
static int gfx_window_close(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_window *win = gfx_toWindow(L, 1);
	gfx_window__gc(L);
	memset(win, 0, sizeof(gfx_window));

	/* if window:close() for the last window is called from an event
	 * handler that is not for the window.close event, then the quit
	 * event does not seem to get generated. So we do it outselves.
	 */
	if (gfx_allWindowsClosed(L)) {
		SDL_Event quit;
		quit.type = SDL_QUIT;
		SDL_PushEvent(&quit);
	}

	return 0;
}

/*** Method
 * Object: window
 * Name: isclosed
 * Synopsis: window:isclosed()
 */
static int gfx_window_isclosed(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_window *win = gfx_toWindow(L, 1);
	lua_pushboolean(L, win->win == 0);
	return 1;
}

/* reset all window properties to defaults
 */
static void gfx_resetWindowProperties(lua_State *L, int idx)
{
	gfx_window *win = (gfx_window*) lua_touserdata(L, idx);
	gfx_matrix_loadidentity(&win->modelview);
	win->fgcolor = (gfx_color){ 1, 1, 1, 1 };
	win->bgcolor = (gfx_color){ 0, 0, 0, 1 };

	// set default shader
	if (lua_getfield(L, idx, "setshader") == LUA_TFUNCTION) {
		lua_pushvalue(L, idx);
		lua_pushnil(L);
		lua_call(L, 2, 0);
	} else {
		luaL_error(L, "(internal error)");
	}
}

/*** Method
 * Object: window
 * Name: update
 * Synopsis: window:update()
 */
static int gfx_window_update(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	if (win->mode == DOUBLE) {
		SDL_GL_SwapWindow(win->win);
	} else {
		glFinish();
	}
	gfx_resetWindowProperties(L, 1);
	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: getsize
 * Synopsis: w, h, d = window:getsize()
 */
static int gfx_window_getsize(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_window *win = gfx_checkAndUseWindow(L, 1);
	if (win->fbo == 0) {
		int dim[4];
		glGetIntegerv(GL_VIEWPORT, dim);
		lua_pushinteger(L, dim[2]);
		lua_pushinteger(L, dim[3]);
		lua_pushinteger(L, 4);
	} else {
		int w, h, d;
		gfx_getProperty(L, 1, WINDOW_PROP_FRAMEBUFFER);
		gfx_image *img = gfx_checkImage(L, lua_gettop(L));
		lua_pop(L, 1);
		gfx_imageSize(img, &w, &h, &d);
		lua_pushinteger(L, w);
		lua_pushinteger(L, h);
		lua_pushinteger(L, d);
	}
	return 3;
}

/*** Method
 * Object: window
 * Name: seteventhandler
 * Synopsis: window:seteventhandler(func)
 */
static int gfx_window_seteventhandler(lua_State *L)
{
	gfx_check_nargs(1, 0, 1);
	gfx_checkAndUseWindow(L, 1);
	if (lua_gettop(L) > 1) {
		if (!lua_isnil(L, 2)) {
			luaL_checktype(L, 2, LUA_TFUNCTION);
		}
	} else {
		lua_pushnil(L);
	}
	gfx_setProperty(L, 1, 2, WINDOW_PROP_HANDLER);
	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: geteventhandler
 * Synopsis: handler = window:geteventhandler()
 */
static int gfx_window_geteventhandler(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_checkAndUseWindow(L, 1);
	gfx_getProperty(L, 1, WINDOW_PROP_HANDLER);
	return 1;
}

static int event_needs_resize(lua_State *L, int idx)
{
	int needs = 0;
	int t = lua_getfield(L, idx, "type");
	if (t == LUA_TSTRING) {
		const char *type = lua_tostring(L, -1);
		if (strcmp(type, "window") == 0) {
			needs = 1;
		}
	}
	lua_pop(L, 1);
	if (!needs) {
		return 0;
	}

	needs = 0;
	t = lua_getfield(L, idx, "which");
	if (t == LUA_TSTRING) {
		const char *type = lua_tostring(L, -1);
		if (strcmp(type, "resize") == 0) {
			needs = 1;
		}
		// TODO others?
	}
	lua_pop(L, 1);
	return needs;
}

/* calls a windows event handler, if present. Returns 1 if the event was
 * handled, 0 otherwise. Invoke with event table, window on stack, pops
 * everything except for the event table from the stack.
 */
int gfx_window_invokehandler(lua_State *L)
{
	int pos = lua_gettop(L) - 1;
	luaL_checktype(L, pos, LUA_TTABLE);
	gfx_window *win = gfx_checkWindow(L, pos+1);
	if (event_needs_resize(L, pos) && win->fbo == 0) {
		gfx_resizeWindow(win);
	}

	if (gfx_getProperty(L, pos+1, WINDOW_PROP_HANDLER) == LUA_TNIL) {
		lua_pop(L, 2);
		return 0;
	}
	lua_pushvalue(L, pos+1);
	lua_pushvalue(L, pos);
	lua_call(L, 2, 1);
	int handled = lua_toboolean(L, -1);
	lua_pop(L, 2);
	return handled;
}

/*** Method
 * Object: window
 * Name: setframebuffer
 * Synopsis: window:setframebuffer(image)
 */
static int gfx_window_setframebuffer(lua_State *L)
{
	gfx_check_nargs(1, 1, 1);
	(void) gfx_checkAndUseWindow(L, 1);
	if (lua_gettop(L) > 1) {
		if (!lua_isnil(L, 2)) {
			(void) gfx_checkImage(L, 2);
		}
	} else {
		lua_pushnil(L);
	}
	gfx_setProperty(L, 1, 2, WINDOW_PROP_FRAMEBUFFER);
	gfx_setupFrameBuffer(L, 1);
	lua_pushvalue(L, 1);
	return 1;
}

/*** Method
 * Object: window
 * Name: getframebuffer
 * Synopsis: image = window:getframebuffer()
 */
static int gfx_window_getframebuffer(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	(void) gfx_checkAndUseWindow(L, 1);
	gfx_getProperty(L, 1, WINDOW_PROP_FRAMEBUFFER);
	return 1;
}

/* methods for the window userdata
 */
static const luaL_Reg gfx_window_methods[] = {
	{"hide", gfx_window_hide},
	{"show", gfx_window_show},
	{"close", gfx_window_close},
	{"isclosed", gfx_window_isclosed},
	{"update", gfx_window_update},
	{"getsize", gfx_window_getsize},
	{"seteventhandler", gfx_window_seteventhandler},
	{"geteventhandler", gfx_window_geteventhandler},
	{"setframebuffer", gfx_window_setframebuffer},
	{"getframebuffer", gfx_window_getframebuffer},
	{0, 0}
};

/*** Constructor
 * Object: window
 * Name: window
 * Synopsis: window = gfx.window(title = "(gfx window)", width = 800, height = 600, flags...)
 * flags may be
 * 	rendering mode: "single", "double", "direct"
 *	"resizable"
 */
static int gfx_newwindow(lua_State *L)
{
	gfx_check_nargs(0, 0, 10);
	static int glew_is_initialized = 0;
	const char *title = luaL_optstring(L, 1, "(gfx window)");
	int width = luaL_optinteger(L, 2, 800);
	int height = luaL_optinteger(L, 3, 600);
	gfx_rendering_mode mode = DIRECT;
	int resizable = 0;
	
	for (int n = 4; n <= lua_gettop(L); ++n) {
		const char *flag = luaL_checkstring(L, n);
		if (strcmp(flag, "double") == 0) {
			mode = DOUBLE;
		} else if (strcmp(flag, "single") == 0) {
			mode = SINGLE;
		} else if (strcmp(flag, "direct") == 0) {
			mode = DIRECT;
		} else if (strcmp(flag, "resizable") == 0 || strcmp(flag, "resizeable") == 0) {
			resizable = 1;
		} else {
			return luaL_argerror(L, n, "invalid flag");
		}
	}

	uint32_t flags = SDL_WINDOW_OPENGL;
	if (resizable) {
		flags |= SDL_WINDOW_RESIZABLE;
	}

	gfx_window *win = gfx_pushWindow(L);
	win->mode = mode;

	win->win = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, flags);
	if (!win->win) {
		return gfx_sdlError(L, "can't create window");
	}
	win->id = SDL_GetWindowID(win->win);

	// set opengl stuff
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, mode == DOUBLE ? 1 : 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);

	win->ctx = SDL_GL_CreateContext(win->win);
	if (!win->ctx) {
		SDL_DestroyWindow(win->win);
		win->win = 0;
		return gfx_sdlError(L, "can't create window");
	}

	if (!glew_is_initialized) {
		glewExperimental = GL_TRUE; // needed for older versions?
		GLenum err = glewInit();
		if (err != GLEW_OK) {
			return luaL_error(L, "can't create window: %s", (const char*)glewGetErrorString(err));
		}
	}
	glGetError(); // clear any errors from glew

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glGenVertexArrays(1, &win->vao);
	glGenBuffers(1, &win->vbo);
	win->fbo = 0;

	// set up defaults
	gfx_resizeWindow(win);
	gfx_resetWindowProperties(L, lua_gettop(L));

	gfx_registerWindow(L, -1);

	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);
	if (lua_getfield(L, -1, "update") == LUA_TFUNCTION) {
		lua_pushvalue(L, -2);
		lua_call(L, 1, 0);
	} else {
		return luaL_error(L, "(internal error)");
	}
	return 1;
}

static const luaL_Reg gfx_window_funcs[] = {
	{"window", gfx_newwindow},
	{0, 0}
};

int gfx_registerWindowFunctions(lua_State *L)
{
	/* add gfx_window userdata metatable */
	luaL_newmetatable(L, GFX_WINDOW);
	luaL_setfuncs(L, gfx_window_meta, 0);
	lua_pushliteral(L, "__index");
	lua_newtable(L);
	luaL_setfuncs(L, gfx_window_methods, 0);
	gfx_registerShaderMethods(L);
	gfx_registerDrawMethods(L);
	gfx_registerImageMethods(L);
	gfx_registerFontMethods(L);
	lua_rawset(L, -3);
	lua_pop(L, 1);

	/* create window registry */
	lua_pushstring(L, GFX_WINDOW_REGISTRY);
	lua_newtable(L);
	lua_settable(L, LUA_REGISTRYINDEX);
	gfx_registerFinalizer(gfx_finalizeWindowRegistry);

	/* register functions */
	luaL_setfuncs(L, gfx_window_funcs, 0);
	gfx_registerEventFunctions(L);

	return 1;
}