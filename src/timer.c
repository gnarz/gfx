/* timer.c
 *
 * timers for gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"

uint32_t GFX_TIMEREVENT = 0;

static uint32_t gfx_timerCallbackfunc(uint32_t interval, void *param)
{
    SDL_Event evt;

    evt.type = GFX_TIMEREVENT;
    evt.user.code = (int32_t) interval;
    evt.user.timestamp = SDL_GetTicks();
    evt.user.windowID = 0;
    evt.user.data1 = param;
    evt.user.data2 = 0;

    SDL_PushEvent(&evt);
    return 0;
}

static void gfx_setTimerRegistry(lua_State *L, const char *name, int id, int pos)
{
	lua_getfield(L, LUA_REGISTRYINDEX, GFX_TIMER_REGISTRY);

	lua_pushinteger(L, id);
	lua_setfield(L, -2, name);

	lua_pushvalue(L, pos);
	lua_rawseti(L, -2, id);

	lua_pop(L, 1);
}

static void gfx_clearTimerRegistry(lua_State *L, const char *name, int id)
{
	lua_getfield(L, LUA_REGISTRYINDEX, GFX_TIMER_REGISTRY);

	lua_pushnil(L);
	lua_setfield(L, -2, name);

	lua_pushnil(L);
	lua_rawseti(L, -2, id);

	lua_pop(L, 1);
}

static int gfx_registerTimer(lua_State *L, uint32_t ms, int pos)
{
	int pushed = 0;
	void *param = NULL;
	const int namelen = 2 + sizeof(void*)*2 + 1;
	char name[namelen];
	if (lua_isstring(L, pos)) {
		param = (void*) lua_tostring(L, pos); // is this ok?
	} else {
		param = (void*) lua_topointer(L, pos);
	}
	snprintf(name, namelen, "%p", param);

	lua_getfield(L, LUA_REGISTRYINDEX, GFX_TIMER_REGISTRY);
	if (lua_getfield(L, -1, name) == LUA_TNIL) {
		int id = SDL_AddTimer(ms, gfx_timerCallbackfunc, param);
		gfx_setTimerRegistry(L, name, id, pos);
		pushed = 1;
	}
	lua_pop(L, 1);
	return pushed;
}

void gfx_unregisterTimer(lua_State *L, void *param)
{
	const int namelen = 2 + sizeof(void*)*2 + 1;
	char name[namelen];
	snprintf(name, namelen, "%p", param);

	lua_getfield(L, LUA_REGISTRYINDEX, GFX_TIMER_REGISTRY);
	lua_getfield(L, -1, name);
	int id = lua_tointeger(L, -1);
	lua_pop(L, 2);

	SDL_RemoveTimer(id);

	gfx_clearTimerRegistry(L, name, id);
}

int gfx_getTimerData(lua_State *L, void *param)
{
	const int namelen = 2 + sizeof(void*)*2 + 1;
	char name[namelen];
	snprintf(name, namelen, "%p", param);

	lua_getfield(L, LUA_REGISTRYINDEX, GFX_TIMER_REGISTRY);
	lua_getfield(L, -1, name);
	int id = lua_tointeger(L, -1);
	lua_pop(L, 1);

	if (lua_rawgeti(L, -1, id) != LUA_TNIL) {
		lua_replace(L, -2);
		return 1;
	}
	lua_pop(L, 2);
	return 0;
}

/*** Function
 * Name: settimer
 * Synopsis: ok = gfx.settimer(interval, data)
 */
static int gfx_settimer(lua_State *L)
{
	gfx_check_nargs(0, 2, 2);
	uint32_t ms = luaL_checkinteger(L, 1);
	if (lua_isnoneornil(L, 2)) {
		return luaL_argerror(L, 2, "timer data missing");
	} else if (!lua_isstring(L, 2) && !lua_topointer(L, 2)) {
		return luaL_argerror(L, 2, "invalid timer data");
	}
	lua_pushboolean(L, gfx_registerTimer(L, ms, 2));
	return 1;
}

/*** Function
 * Name: cleartimer
 * Synopsis: gfx.cleartimer(data)
 */
static int gfx_cleartimer(lua_State *L)
{
	gfx_check_nargs(0, 1, 1);
	void *param = NULL;
	if (lua_isnoneornil(L, 1)) {
		return luaL_argerror(L, 1, "timer data missing");
	} else if (lua_isstring(L, 1)) {
		param = (void*) lua_tostring(L, 1); // is this ok?
	} else {
		param = (void*) lua_topointer(L, 1);
	}
	if (!param) {
		return luaL_argerror(L, 1, "invalid timer data");
	}
	gfx_unregisterTimer(L, param);
	return 0;
}

static const luaL_Reg gfx_timer_funcs[] = {
	{"settimer", gfx_settimer},
	{"cleartimer", gfx_cleartimer},
	{0, 0}
};

int gfx_registerTimerFunctions(lua_State *L)
{
	GFX_TIMEREVENT = SDL_RegisterEvents(1);
	/* create timer registry */
	lua_pushstring(L, GFX_TIMER_REGISTRY);
	lua_newtable(L);
	lua_settable(L, LUA_REGISTRYINDEX);

	luaL_setfuncs(L, gfx_timer_funcs, 0);

	return 1;
}
