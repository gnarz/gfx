gfx = require "gfx"
win = gfx.window("Mandelbrot set", 800, 600, "single")

local timeslice = 100 -- ms

local minx = -2
local maxx = 1
local miny = -1.3
local maxy = 1.3
local niter = 64

local function make_palette(max, r1, g1, b1, r2, g2, b2, ...)
	local pal = {}

	local function interpolate(s, e, r1, g1, b1, r2, g2, b2)
		if e == s then
			pal[#pal+1] = {r2, g2, b2}
			return
		end
		local range = e - s
		for n=0, range do
			local fac = n / range
			local rfac = 1 - fac
			pal[#pal+1] = { rfac * r1 + fac * r2, rfac * g1 + fac * g2, rfac * b1 + fac * b2 }
		end
	end

	local n = 1
	local nopt = select('#', ...)
	if nopt > 0 and nopt % 3 ~= 0 then
		error("you must specify r, g, b values for all palette steps")
	end
	n = n + nopt // 3
	local cols  = { r1, g1, b1, r2, g2, b2, ... }
	local s, e = 1, 0
	for i = 1, n do
		local pos = (i - 1) * 3 + 1
		e = max * i // n
		interpolate(s, e, cols[pos], cols[pos+1], cols[pos+2], cols[pos+3], cols[pos+4], cols[pos+5])
		s = e + 1
	end
	return pal
end

local function iter_pt(cx, cy, maxi)
	local bq = 0
	local iter = 0
	local x = 0
	local y = 0
	local xt, yt

	while (bq <= 2) and (iter < maxi) do
		xt = x * x - y * y + cx
		yt = 2 * x * y + cy
		x = xt
		y = yt
		iter = iter + 1
		bq = x * x + y * y
	end

	return iter
end

local function create_mb_renderer(minx, maxx, miny, maxy, maxi, pal)
	maxi = maxi or 512
	pal = pal or make_palette(maxi, 0, 0, 1, 0, 1, 0)
	local w, h = win:getsize()
	local xs = (maxx - minx) / w
	local ys = (maxy - miny) / h
	local tm = gfx.getms()

	win:clear(0, 0, 0)

	local x, y, cx, cy, iw, nt
	

	return coroutine.wrap(
		function()
			for x = 0, w - 1 do
				cx = minx + x * xs
		 
				for y = 0, h - 1 do
					cy = miny + y * ys
		 
					iw = iter_pt (cx, cy, maxi)
		 
					if iw >= maxi then
						win:setfg(0, 0, 0)
					else
						win:setfg(pal[iw][1], pal[iw][2], pal[iw][3])
					end
		 
					win:point(x, y)

					if gfx.getms() - tm > timeslice then
						win:update()
						coroutine.yield(true)
						tm = gfx.getms()
					end
				end
			end
		end
	)
end

local pal = make_palette(niter, 0, 0, 1, 0, 1, 0, 1, 0, 0)
do_render = create_mb_renderer(-2.1, 1.3, -1.3, 1.3, niter, pal)

local evt
local quit = false
local render = true
repeat
	if render then
		render = do_render()
		if not render then
			local img = win:grabimage()
			img:save("mandelbrot_out.png")
		end
	end
	evt = gfx.pollevent(not render)
	if evt then
		if evt.type == "window" and evt.which == "close" then
			quit = true
		elseif evt.type == "key" and evt.key == "Escape" then
			quit = true
		end
	end
until quit
