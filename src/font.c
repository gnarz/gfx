/* font.c
 *
 * font handling and text rendering for gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"
#include "mini_utf8.h"

/*** Module
 * Name: gfx
 */

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#include "builtinfont.inc"

#include <string.h>

static const char *font_fragment_shader = GLSL(150,
	in vec2 itexc;
	uniform vec4 fgcolor;
	uniform sampler2D texture0;
	out vec4 fragColor;
	void main() {
		fragColor = vec4(fgcolor.r, fgcolor.g, fgcolor.b, texture(texture0, itexc).r);
	}
);

/*** Object
 * Name: glyph
 * just a wrapper around a glyph structure
 */

struct gfx_glyph {
	gfx_rect bbox;
	int xpos, advance;	// rendering x offset before and after glyph
	int ypos;			// rendering y offset
};

#define gfx_pushGlyph(L) (struct gfx_glyph*)gfx_pushHelper(L, GFX_GLYPH, sizeof(struct gfx_glyph))
#define gfx_checkGlyph(L, idx) (struct gfx_glyph*)luaL_checkudata(L, idx, GFX_GLYPH)

/* skyline stuff */

struct skylineEntry {
	int x, y, w;
	struct skylineEntry *next;
};

static struct skylineEntry *gfx_fontNewSkylineEntry(int x, int y, int w)
{
	struct skylineEntry *entry = calloc(1, sizeof(struct skylineEntry));
	assert(entry != NULL);
	entry->x = x;
	entry->y = y;
	entry->w = w;
	return entry;
}

static void gfx_fontDeleteSkyline(gfx_font *fnt)
{
	struct skylineEntry *entry = fnt->skyline, *next = NULL;
	while (entry) {
		next = entry->next;
		free(entry);
		entry = next;
	}
	fnt->skyline = NULL;
}

static void gfx_fontInitSkyline(gfx_font *fnt, int w)
{
	fnt->skyline = gfx_fontNewSkylineEntry(0, 0, w);
	fnt->skyline->next = NULL;
}

static void gfx_fontGrowSkyline(gfx_font *fnt, int w)
{
	struct skylineEntry *entry = fnt->skyline, *last = 0;
	int owidth = 0;
	while (entry) {
		owidth += entry->w;
		last = entry;
		entry = entry->next;
	}
	last->next = gfx_fontNewSkylineEntry(owidth, 0, w - owidth);
}

static void gfx_fontUpdateSkyline(struct skylineEntry *entry, int w, int h)
{
	if (entry->w > w) {
		struct skylineEntry *new = gfx_fontNewSkylineEntry(entry->x + w, entry->y, entry->w - w);
		new->next = entry->next;
		entry->next = new;
		entry->w = w;
		entry->y += h;
	} else if (entry->w == w) {
		entry->y += h;
	} else {
		struct skylineEntry *next = entry->next;
		int ow = w - entry->w;
		entry->w = w;
		entry->y += h;
		while (next->w <= ow) {
			struct skylineEntry *this = next;
			ow -= this->w;
			next = this->next;
			free(this);
		}
		entry->next = next;
		if (ow > 0) {
			next->w -= ow;
			next->x += ow;
		}
	}
}

static int gfx_fontSkylineRectFits(gfx_font *fnt, struct skylineEntry *entry, int w, int h)
{
	int sh = entry->y + 1;
	if (entry->y + h >= fnt->bm_height || entry->x + w >= fnt->bm_width) {
		return 0;
	}
	do {
		if (entry->y >= sh) {
			return 0;
		}
		w = w - entry->w;
		entry = entry->next;
	} while (w > 0 && entry != NULL);
	return 1;
}

static int gfx_fontAddToSkyline(gfx_font *fnt, int w, int h, gfx_rect *res)
{
	int top = fnt->bm_height + 1;
	struct skylineEntry *entry = fnt->skyline, *pos = NULL;
	while (entry) {
		if (gfx_fontSkylineRectFits(fnt, entry, w, h) && h + entry->y < top) {
			pos = entry;
			top = h + entry->y;
		}
		entry = entry->next;
	}

	if (pos != NULL) {
		gfx_fontUpdateSkyline(pos, w, h);
		res->w = w;
		res->h = h;
		res->x = pos->x;
		res->y = pos->y - h;
		return 1;
	}
	return 0;
}

static struct gfx_glyph *gfx_fontAddGlyph(lua_State *L, int idx, int cp, int size, const struct gfx_glyph glyph)
{
	gfx_getProperty(L, idx, FONT_PROP_GLYPHMAP);
	if (lua_rawgeti(L, -1, size) != LUA_TTABLE) {
		lua_pop(L, 1);
		lua_newtable(L);
		lua_pushvalue(L, -1);
		lua_rawseti(L, -3, size);
	}
	struct gfx_glyph *val = gfx_pushGlyph(L);
	*val = glyph;
	lua_rawseti(L, -2, cp);
	lua_pop(L, 2);
	return val;
}

static struct gfx_glyph *gfx_fontGetGlyph(lua_State *L, int idx, int cp, int size)
{
	struct gfx_glyph *glyph = NULL;
	gfx_getProperty(L, idx, FONT_PROP_GLYPHMAP);
	if (lua_rawgeti(L, -1, size) != LUA_TTABLE) {
		lua_pop(L, 2);
		return NULL;
	}
	lua_rawgeti(L, -1, cp);
	if (!lua_isnil(L, -1)) {
		glyph = gfx_checkGlyph(L, -1);
	}
	lua_pop(L, 3);
	return glyph;
}

/*** Module
 * Name: gfx
 */

/*** Object
 * Name: font
 */

#define gfx_checkFont(L, index) luaL_checkudata(L, index, GFX_FONT);

static gfx_font* gfx_pushFont(lua_State *L)
{
	gfx_font *fnt = (gfx_font*) lua_newuserdata(L, sizeof(gfx_font));
	memset(fnt, 0, sizeof(gfx_font));
	luaL_getmetatable(L, GFX_FONT);
	lua_setmetatable(L, -2);
	lua_newtable(L);
	lua_setuservalue(L, -2);
	return fnt;
}

static int gfx_font__gc(lua_State *L)
{
	gfx_font *fnt = (gfx_font*) lua_touserdata(L, 1);
	if (!fnt->is_builtin) {
		if (fnt->data) { free((void*)fnt->data); }
		if (fnt->info) { free(fnt->info); }
		if (fnt->bitmap) { free(fnt->bitmap); }
		if (fnt->skyline) { gfx_fontDeleteSkyline(fnt); }
	}
	return 0;
}

static int gfx_font__toString(lua_State *L)
{
	void *win = (gfx_font*) lua_touserdata(L, 1);
	lua_pushfstring(L, "%s: %p", GFX_FONT, win);
	return 1;
}

static const luaL_Reg gfx_font_meta[] = {
	{"__gc", gfx_font__gc},
	{"__tostring", gfx_font__toString},
	{0, 0}
};

/* bitmap cache stuff */

static int gfx_fontInitBitmap(gfx_font *fnt, int size)
{
	fnt->bitmap = calloc(size * size, 1);
	assert(fnt->bitmap != NULL);
	fnt->bm_width = size;
	fnt->bm_height = size;
	return 1;
}

static int gfx_fontGrowBitmap(gfx_font *fnt)
{
	if (fnt->bm_width > 32767 && fnt->bm_height > 32767) {
		return 0;
	}
	int newwidth = fnt->bm_width;
	int newheight = fnt->bm_height;
	if (newwidth == newheight) {
		newwidth *= 2;
	} else {
		newheight *= 2;
	}
	uint8_t *newbm = calloc(newwidth * newheight, 1);
	if (newbm) {
		for (int y = 0; y < fnt->bm_height; ++y) {
			memcpy(newbm + y * newwidth, fnt->bitmap + y * fnt->bm_width, fnt->bm_width);
		}
		free(fnt->bitmap);
		fnt->bitmap = newbm;
		if (newwidth > fnt->bm_width) {
			gfx_fontGrowSkyline(fnt, newwidth);
		}
		fnt->bm_width = newwidth;
		fnt->bm_height = newheight;
		return 1;
	}
	return 0;
}

static int gfx_fontUpdateTexture(lua_State *L, int idx)
{
	if (idx < 0) {
		idx = lua_gettop(L) + 1 + idx;
	}
	gfx_font *fnt = (gfx_font*) lua_touserdata(L, idx);
	if (gfx_getProperty(L, idx, FONT_PROP_TEXTURE) == LUA_TNIL) {
		gfx_pushNewImage(L, fnt->bm_width, fnt->bm_height, 1, fnt->bitmap);
		gfx_setProperty(L, idx, -1, FONT_PROP_TEXTURE);
		lua_pop(L, 2);
	} else {
		gfx_image *img = gfx_checkImage(L, -1);
		gfx_imageUpdate(img, fnt->bm_width, fnt->bm_height, fnt->bitmap);
		lua_pop(L, 1);
	}
	return 1;
}

static struct gfx_glyph *gfx_fontMakeGlyph(lua_State *L, int idx, int cp, int size)
{
	gfx_font *fnt = (gfx_font*) lua_touserdata(L, idx);
	struct gfx_glyph glyph;
	float scale = stbtt_ScaleForPixelHeight(fnt->info, size);
	int ascent, advance, lsb, x0, y0, x1, y1, gw, gh;
	stbtt_GetFontVMetrics(fnt->info, &ascent,0,0);
	stbtt_GetCodepointHMetrics(fnt->info, cp, &advance, &lsb);
	stbtt_GetCodepointBitmapBox(fnt->info, cp, scale, scale, &x0, &y0, &x1, &y1);
	gw = x1 - x0;
	gh = y1 - y0;
	glyph.ypos = floorf((ascent*scale) + y0 + 0.5);
	glyph.xpos = x0;
	glyph.advance = advance * scale;

	while (gfx_fontAddToSkyline(fnt, gw + 1, gh + 1, &glyph.bbox) == 0) {
		int ok = gfx_fontGrowBitmap(fnt);
		if (!ok) {
			/* return */ luaL_error(L, "can't grow backing bitmap any more");
		}
	}
	glyph.bbox.w = gw;
	glyph.bbox.h = gh;
	struct gfx_glyph *g = gfx_fontAddGlyph(L, idx, cp, size, glyph);

	uint8_t *out_pos = fnt->bitmap + (int)glyph.bbox.y * fnt->bm_width + (int)glyph.bbox.x;
	stbtt_MakeCodepointBitmap(fnt->info, out_pos, gw, gh, fnt->bm_width, scale, scale, cp);
	
	gfx_fontUpdateTexture(L, idx);

	return g;
}

static struct gfx_glyph *gfx_fontFindGlyph(lua_State *L, int idx, int cp, int size)
{
	struct gfx_glyph *g = gfx_fontGetGlyph(L, idx, cp, size);
	if (!g) {
		g = gfx_fontMakeGlyph(L, idx, cp, size);
	}
	return g;
}

static int gfx_fontRenderString(lua_State *L, int idx, const char *str, int size, float x, float y, int ncoords, gfx_vec2 *coords, gfx_vec2 *texcs)
{
	assert((ncoords >= 0) && (ncoords >= mini_utf8_strlen(str) * 6));

	int cp = mini_utf8_decode(&str), n = 0;
	while (cp > 0) {
		struct gfx_glyph *glyph = gfx_fontFindGlyph(L, idx, cp, size);

		gfx_rect pos, texc;
		pos.x = x + glyph->xpos;
		pos.y = y + glyph->ypos;
		pos.w = glyph->bbox.w + 1;
		pos.h = glyph->bbox.h + 1;
		texc.x = glyph->bbox.x;
		texc.y = glyph->bbox.y + glyph->bbox.h + 0.5;
		texc.w = glyph->bbox.w + 0.5;
		texc.h = -glyph->bbox.h + 0.5;

		gfx_makeTexturedRectangle(&pos, &texc, &coords[n * 6], &texcs[n * 6]);

		x += glyph->advance;
		n += 1;
		cp = mini_utf8_decode(&str);
	}
	if (cp < 0) {
		return luaL_error(L, "invalid utf8 string.");
	}
	// fix texture coordinates
	gfx_font *fnt = (gfx_font*) lua_touserdata(L, idx);
	float bw = fnt->bm_width, bh = fnt->bm_height;
	for (int i = 0; i < n * 6; ++i) {
		texcs[i].x /= bw;
		texcs[i].y /= bh;
	}
	return n * 6;
}

static int gfx_fontRendersize(lua_State *L, int idx, const char *str, int size, gfx_rect *res)
{
	assert(str && (size > 0));
	int cp = mini_utf8_decode(&str);
	res->x = 0;
	res->y = 0;
	res->w = 0;
	res->h = size;
	while (cp > 0) {
		struct gfx_glyph *glyph = gfx_fontFindGlyph(L, idx, cp, size);
		res->w += glyph->advance;
		cp = mini_utf8_decode(&str);
	}
	if (cp < 0) {
		return luaL_error(L, "invalid utf8 string.");
	}
	return 1;
}

/*** Method
 * Object: font
 * Name: render
 * Synopsis: vec, tex = font:render("string", size, x = 0, y = 0)
 */
static int gfx_font_render(lua_State *L)
{
	gfx_check_nargs(1, 2, 4);
	(void) gfx_checkFont(L, 1);
	const char *str = luaL_checkstring(L, 2);
	int size = luaL_optinteger(L, 3, GFX_DEFAULT_FONT_SIZE);
	float x = luaL_optnumber(L, 4, 0);
	float y = luaL_optnumber(L, 5, 0);

	int ncoords = mini_utf8_strlen(str) * 6;
	if (ncoords < 0) {
		return luaL_error(L, "invalid utf8 string.");
	}

	gfx_vec2 coords[ncoords], texcs[ncoords];

	ncoords = gfx_fontRenderString(L, 1, str, size, x, y, ncoords, coords, texcs);

	lua_newtable(L);	// return coords
	lua_newtable(L);	// return texcs
	for (int i = 0; i < ncoords; ++i) {
		lua_pushnumber(L, coords[i].x);
		lua_rawseti(L, -3, i * 2 + 1);
		lua_pushnumber(L, coords[i].y);
		lua_rawseti(L, -3, i * 2 + 2);

		lua_pushnumber(L, texcs[i].x);
		lua_rawseti(L, -2, i * 2 + 1);
		lua_pushnumber(L, texcs[i].y);
		lua_rawseti(L, -2, i * 2 + 2);
	}

	return 2;
}

/*** Method
 * Object: font
 * Name: rendersize
 * Synopsis: w, h = font:rendersize("string", size)
 */
static int gfx_font_rendersize(lua_State *L)
{
	gfx_check_nargs(1, 2, 2);
	(void) gfx_checkFont(L, 1);
	const char *str = luaL_checkstring(L, 2);
	int size = luaL_optinteger(L, 3, GFX_DEFAULT_FONT_SIZE);
	gfx_rect res;
	gfx_fontRendersize(L, 1, str, size, &res);
	lua_pushnumber(L, res.w);
	lua_pushnumber(L, res.h);
	return 2;
}

/*** Method
 * Object: font
 * Name: hasglyph
 * Synopsis: ok = font:hasglyph(cp)
 */
static int gfx_font_hasglyph(lua_State *L)
{
	gfx_check_nargs(1, 1, 1);
	gfx_font *fnt = gfx_checkFont(L, 1);
	int cp;
	if (lua_isinteger(L, 2)) {
		cp = luaL_checkinteger(L, 2);
	} else if (lua_isstring(L, 2)) {
		const char *str = lua_tostring(L, 2);
		cp = mini_utf8_decode(&str);
	} else {
		return luaL_argerror(L, 2, "number or string expected");
	}
	int ok = stbtt_FindGlyphIndex(fnt->info, cp);
	lua_pushboolean(L, ok != 0);
	return 1;
}

/*** Method
 * Object: font
 * Name: gettexture
 * Synopsis: image = font:gettexture()
 */
static int gfx_font_gettexture(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_getProperty(L, 1, FONT_PROP_TEXTURE);
	return 1;
}

#ifdef DEBUGGING
/* ** Method
 * Object: font
 * Name: getskyline
 * Synopsis: {x,y,w,...} = font:getskyline()
 */
static int gfx_font_getskyline(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	gfx_font *fnt = gfx_checkFont(L, 1);
	struct skylineEntry *entry = fnt->skyline;
	lua_newtable(L);
	int idx = 1;
	while (entry) {
		lua_pushinteger(L, entry->x);
		lua_rawseti(L, -2, idx++);
		lua_pushinteger(L, entry->y);
		lua_rawseti(L, -2, idx++);
		lua_pushinteger(L, entry->w);
		lua_rawseti(L, -2, idx++);
		entry = entry->next;
	}
	return 1;
}
#endif

static const luaL_Reg gfx_font_methods[] = {
	{"render", gfx_font_render},
	{"rendersize", gfx_font_rendersize},
	{"hasglyph", gfx_font_hasglyph},
	{"gettexture", gfx_font_gettexture},
#ifdef DEBUGGING
	{"getskyline", gfx_font_getskyline},
#endif
	{0, 0}
};

/*** Method
 * Object: window
 * Name: setfont
 * Synopsis: window:setfont(font = nil, size = nil)
 */
static int gfx_window_setfont(lua_State *L)
{
	gfx_check_nargs(1, 0, 2);
	(void) gfx_checkWindow(L, 1);
	if (lua_gettop(L) < 2) {
		lua_pushnil(L);
	}
	if (lua_gettop(L) < 3) {
		lua_pushnil(L);
	}

	if (!lua_isnil(L, 2)) {
		(void) gfx_checkFont(L, 2);
	} else {
		lua_pushstring(L, GFX_BUILTIN_FONT);
		lua_gettable(L, LUA_REGISTRYINDEX);
		lua_replace(L, 2);
	}
	gfx_setProperty(L, 1, 2, WINDOW_PROP_FONT);
	if (!lua_isnil(L, 3)) {
		luaL_checkinteger(L, 3);
	} else {
		lua_pushinteger(L, GFX_DEFAULT_FONT_SIZE);
		lua_replace(L, 3);
	}
	gfx_setProperty(L, 1, 3, WINDOW_PROP_FONTSIZE);
	lua_pushvalue(L, 1);
	return 1;
}

static int gfx_windowGetFont(lua_State *L)
{
	(void) gfx_checkWindow(L, 1);
	if (gfx_getProperty(L, 1, WINDOW_PROP_FONT) == LUA_TNIL) {
		lua_pop(L, 1);
		lua_pushstring(L, GFX_BUILTIN_FONT);
		lua_gettable(L, LUA_REGISTRYINDEX);
	}
	if (gfx_getProperty(L, 1, WINDOW_PROP_FONTSIZE) == LUA_TNIL) {
		lua_pop(L, 1);
		lua_pushinteger(L, GFX_DEFAULT_FONT_SIZE);
	}
	return 2;
}

/*** Method
 * Object: window
 * Name: getfont
 * Synopsis: font, size = window:getfont()
 */
static int gfx_window_getfont(lua_State *L)
{
	gfx_check_nargs(1, 0, 0);
	return gfx_windowGetFont(L);
}

static int gfx_pushFontShader(lua_State *L)
{
	static GLuint font_shader = 0;
	if (font_shader == 0) {
		font_shader = gfx_shaderBuild(L, font_fragment_shader);
	}
	gfx_shader *sh = gfx_pushShader(L);
	sh->destroy = NULL; // don't destroy default shaders!
	sh->id = font_shader;
	return 1;
}

/*** Method
 * Object: window
 * Name: print
 * Synopsis: window:print(str, x = 0, y = 0)
 */
static int gfx_window_print(lua_State *L)
{
	gfx_check_nargs(1, 1, 3);
	(void) gfx_checkWindow(L, 1);
	const char *str = luaL_checkstring(L, 2);
	float x = luaL_optnumber(L, 3, 0);
	float y = luaL_optnumber(L, 4, 0);
	gfx_windowGetFont(L);
	int size = lua_tointeger(L, -1);
	lua_pop(L, 1);
	int fontpos = lua_gettop(L);

	int ncoords = mini_utf8_strlen(str) * 6;
	if (ncoords < 0) {
		return luaL_error(L, "invalid utf8 string.");
	}
	gfx_vec2 coords[ncoords], texcs[ncoords];
	ncoords = gfx_fontRenderString(L, -1, str, size, x, y, ncoords, coords, texcs);

	gfx_getProperty(L, 1, WINDOW_PROP_TEXTURE0);
	int txpos = lua_gettop(L);
	gfx_getProperty(L, fontpos, FONT_PROP_TEXTURE);
	gfx_setProperty(L, 1, -1, WINDOW_PROP_TEXTURE0);
	lua_pop(L, 1);
	
	gfx_getProperty(L, 1, WINDOW_PROP_SHADER);
	int shpos = lua_gettop(L);
	gfx_pushFontShader(L);
	gfx_setProperty(L, 1, -1, WINDOW_PROP_SHADER);
	lua_pop(L, 1);

	gfx_drawstuff(L, 1, GL_TRIANGLES, ncoords, coords, texcs, NULL);

	gfx_setProperty(L, 1, shpos, WINDOW_PROP_SHADER);
	gfx_setProperty(L, 1, txpos, WINDOW_PROP_TEXTURE0);
	lua_pop(L, 3);

	lua_pushvalue(L, 1);
	return 1;
}

static const luaL_Reg gfx_font_window_methods[] = {
	{"setfont", gfx_window_setfont},
	{"getfont", gfx_window_getfont},
	{"print", gfx_window_print},
	{0, 0}
};

static gfx_font *gfx_pushNewFont(lua_State *L, const uint8_t *data)
{
	gfx_font *fnt = gfx_pushFont(L);
	fnt->data = data;
	fnt->info = malloc(sizeof(stbtt_fontinfo));
	assert(fnt->info != NULL);
	if (stbtt_InitFont(fnt->info, fnt->data, 0) == 0) {
		/* return */ luaL_error(L, "failed to initialize builtin font");
	}
	gfx_fontInitBitmap(fnt, GFX_DEFAULT_BITMAP_SIZE);
	gfx_fontInitSkyline(fnt, GFX_DEFAULT_BITMAP_SIZE);
	lua_newtable(L);
	gfx_setProperty(L, -2, -1, FONT_PROP_GLYPHMAP);
	lua_pop(L, 1);
	return fnt;
}

/*** Constructor
 * Name: loadfont
 * Synopsis: font = gfx.loadfont(path)
 */
static int gfx_loadfont(lua_State *L)
{
	gfx_check_nargs(0, 1, 1);
	const char *path = luaL_checkstring(L, 1);
	uint8_t *data = NULL;
	FILE *f = fopen(path, "r");
	if (!f) {
		return luaL_error(L, "can't open font file");
	}
	fseek(f, 0, SEEK_END);
	size_t fsize = ftell(f);
	fseek(f, 0, SEEK_SET);
	data = malloc(fsize);
	assert(data != NULL);
	size_t rsize = fread(data, 1, fsize, f);
	fclose(f);
	if (rsize != fsize) {
		free(data);
		return luaL_error(L, "error while reading font file");
	}
	gfx_pushNewFont(L, data);
	return 1;
}

static const luaL_Reg gfx_font_functions[] = {
	{"loadfont", gfx_loadfont},
	{0, 0}
};

static void gfx_initBuiltinFont(lua_State *L)
{
	lua_pushstring(L, GFX_BUILTIN_FONT);
	gfx_font *fnt = gfx_pushNewFont(L, builtin_font_data);
	fnt->is_builtin = 1;
	lua_settable(L, LUA_REGISTRYINDEX);
}

int gfx_registerFontFunctions(lua_State *L)
{
	gfx_createHelperObject(L, GFX_GLYPH);

	luaL_newmetatable(L, GFX_FONT);
	luaL_setfuncs(L, gfx_font_meta, 0);
	lua_pushliteral(L, "__index");
	lua_newtable(L);
	luaL_setfuncs(L, gfx_font_methods, 0);
	lua_rawset(L, -3);
	lua_pop(L, 1);

	gfx_initBuiltinFont(L);

	luaL_setfuncs(L, gfx_font_functions, 0);

	return 1;
}

int gfx_registerFontMethods(lua_State *L)
{
	luaL_setfuncs(L, gfx_font_window_methods, 0);
	return 1;
}