-- test/shader.lua
-- tests for shader functionality
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require"gfx"
win = gfx.window("shader test", 800, 600, "single")

img1 = gfx.loadimage("tests/image1.png")
img2 = gfx.loadimage("tests/image2.png")

sh1 = gfx.shader [[
	in vec2 itexc;
	uniform sampler2D texture0, texture1;
	out vec4 fragColor;
	void main() {
		fragColor = texture(texture0, itexc) + texture(texture1, itexc);
	}
]]
win:setshader(sh1)
win:settexture(img1, 0)
win:settexture(img2, 1)
coords, texc = gfx.gentexturedrect(0, 0, 400, 300)
win:draw("triangles", coords, texc)
win:settexture(nil, 0)
win:settexture(nil, 1)

sh2 = gfx.shader [[
	in vec2 itexc;
	in vec4 icolor;
	uniform sampler2D texture0;
	out vec4 fragColor;
	void main() {
		fragColor = texture(texture0, itexc) + icolor;
	}
]]
win:setshader(sh2)
win:settexture(img1, 0)
coords, texc = gfx.gentexturedrect(400, 0, 400, 300)
cols = {
	1,0,0,1,	-- 0
	1,1,0,1,	-- 1
	1,0,1,1,	-- 2
	1,0,0,1,	-- 0
	1,0,1,1,	-- 2
	0,1,1,1		-- 3
}
win:draw("triangles", coords, texc, cols)
win:settexture(nil, 0)

sh3 = gfx.shader [[
	in vec2 itexc;
	uniform vec4 fgcolor;
	out vec4 fragColor;
	void main() {
		float x = itexc.x - 0.5;
		float y = itexc.y - 0.5;
		float rsq = x*x + y*y;
		fragColor = vec4(fgcolor.r, fgcolor.g, fgcolor.b, rsq < 0.25 ? 1 : 0);
	}
]]
win:setshader(sh3)
win:setfg(0.5, 1, 1)
coords, texc = gfx.gentexturedrect(0, 300, 400, 300)
win:draw("triangles", coords, texc, cols)


sh4 = gfx.shader [[
	in vec2 itexc;
	uniform vec4 fgcolor;
	uniform sampler2D texture0;
	out vec4 fragColor;
	void main() {
		float x = itexc.x - 0.5;
		float y = itexc.y - 0.5;
		float rsq = x*x + y*y;
		if (rsq < 0.2) {
			fragColor = texture(texture0, itexc);
		} else {
			fragColor = vec4(fgcolor.r, fgcolor.g, fgcolor.b, rsq < 0.25 ? 1 : 0);
		}
	}
]]
win:setshader(sh4)
win:settexture(img1, 0)
coords, texc = gfx.gentexturedrect(400, 300, 400, 300)
win:draw("triangles", coords, texc, cols)
win:settexture(img1, nil)

win:update()

win:seteventhandler(function(w, e)
	if e.key == "Escape" then
		w:close()
		return true
	end
end)
gfx.mainloop()
