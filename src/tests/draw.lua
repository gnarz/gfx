-- test/draw.lua
-- tests the drawing functions
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require"gfx"
win = gfx.window("font test", 800, 600, "direct")
img = gfx.loadimage("tests/image2.png")

win:clear(0.1, 0.1, 0.2)

xpos0 = 25
ypos0 = 25
width = 50
height= 50
xstep = 2 * width
ystep = ypos0 + height
_, theight = win:getfont()

xpos = xpos0
ypos = ypos0

-- line
win:setfg(1, 0, 0)
win:print("line", xpos, ypos - theight)
win:line(xpos, ypos, xpos, ypos+height)
win:line(xpos + width, ypos, xpos + width, ypos + height)
win:line(xpos, ypos, xpos + width, ypos)
win:line(xpos, ypos + height, xpos + width, ypos + height)

ypos = ypos + ystep

-- lines
win:setfg(1, 0.5, 0)
coords = {
    xpos, ypos, xpos, ypos+height,
    xpos + width, ypos, xpos + width, ypos + height,
    xpos, ypos, xpos + width, ypos,
    xpos, ypos + height, xpos + width, ypos + height
}
win:print("lines", xpos, ypos - theight)
win:lines(coords, "lines")

ypos = ypos + ystep

-- line strip
win:setfg(1, 0.5, 0.5)
coords = {
    xpos, ypos,
    xpos, ypos+height,
    xpos + width, ypos + height,
    xpos + width, ypos,
    xpos, ypos
}
win:print("strip", xpos, ypos - theight)
win:lines(coords, "strip")

ypos = ypos + ystep

-- line loop
win:setfg(1, 1, 0.5)
coords = {
    xpos, ypos,
    xpos, ypos+height,
    xpos + width, ypos + height,
    xpos + width, ypos
}
win:print("loop", xpos, ypos - theight)
win:lines(coords, "loop")

ypos = ypos + ystep

-- rectangle
win:setfg(1, 0.5, 1)
win:print("rectangle", xpos, ypos - theight)
win:rectangle(xpos, ypos, width, height, false)

ypos = ypos + ystep

-- rectangle fill
win:setfg(1, 1, 1)
win:print("rect fill", xpos, ypos - theight)
win:rectangle(xpos, ypos, width, height, true)

xpos = xpos + xstep
ypos = ypos0

--- point
win:setfg(1, 0, 0)
win:print("point", xpos, ypos - theight)
for i = 0, 50 do
    win:point(xpos + i, ypos)
    win:point(xpos + i, ypos + height)
    win:point(xpos, ypos + i)
    win:point(xpos + width, ypos + i)
end

ypos = ypos + ystep

-- points
win:setfg(1, 0.5, 0)
win:print("points", xpos, ypos - theight)
coords = {}
for i = 0, 50 do
    coords[#coords+1] = xpos + i
    coords[#coords+1] = ypos
    coords[#coords+1] = xpos + i
    coords[#coords+1] = ypos + height
    coords[#coords+1] = xpos
    coords[#coords+1] = ypos + i
    coords[#coords+1] = xpos + width
    coords[#coords+1] = ypos + i
end
win:points(coords)

ypos = ypos + ystep

-- triangle
win:setfg(1, 0.5, 0.5)
win:print("triangle", xpos, ypos - theight)
win:triangle(xpos, ypos, xpos + width, ypos, xpos + width, ypos + height, false)
win:triangle(xpos, ypos, xpos + width, ypos + height, xpos, ypos + height, false)

ypos = ypos + ystep

-- triangle fill
win:setfg(1, 1, 0.5)
win:print("tri fill", xpos, ypos - theight)
win:triangle(xpos, ypos, xpos + width, ypos, xpos + width, ypos + height, true)
win:triangle(xpos, ypos, xpos + width, ypos + height, xpos, ypos + height, true)


xpos = xpos + xstep
ypos = ypos0

-- draw lines
win:setfg(1, 0, 0)
coords = {
    xpos, ypos, xpos, ypos+height,
    xpos + width, ypos, xpos + width, ypos + height,
    xpos, ypos, xpos + width, ypos,
    xpos, ypos + height, xpos + width, ypos + height
}
win:print("draw lines", xpos, ypos - theight)
win:draw("lines", coords)

ypos = ypos + ystep

-- draw line strip
win:setfg(1, 0.5, 0)
coords = {
    xpos, ypos,
    xpos, ypos+height,
    xpos + width, ypos + height,
    xpos + width, ypos,
    xpos, ypos
}
win:print("line_strip", xpos, ypos - theight)
win:draw("line_strip", coords)

ypos = ypos + ystep

-- draw line loop
win:setfg(1, 0.5, 0.5)
coords = {
    xpos, ypos,
    xpos, ypos+height,
    xpos + width, ypos + height,
    xpos + width, ypos
}
win:print("line_loop", xpos, ypos - theight)
win:draw("line_loop", coords)

ypos = ypos + ystep

-- draw points
win:setfg(1, 1, 0.5)
win:print("draw points", xpos, ypos - theight)
coords = {}
for i = 0, 50 do
    coords[#coords+1] = xpos + i
    coords[#coords+1] = ypos
    coords[#coords+1] = xpos + i
    coords[#coords+1] = ypos + height
    coords[#coords+1] = xpos
    coords[#coords+1] = ypos + i
    coords[#coords+1] = xpos + width
    coords[#coords+1] = ypos + i
end
win:draw("points", coords)

ypos = ypos + ystep

-- draw triangles
win:setfg(1, 0.5, 1)
win:print("draw tri", xpos, ypos - theight)
coords = {
    xpos, ypos,
    xpos, ypos + height,
    xpos + width, ypos + height,
    xpos, ypos,
    xpos + width, ypos + height,
    xpos + width, ypos
}
win:draw("triangles", coords)

ypos = ypos + ystep

-- draw triangle strip
win:setfg(1, 1, 1)
win:print("tri_strip", xpos, ypos - theight)
coords = {
    xpos, ypos + height,
    xpos, ypos,
    xpos + width, ypos + height,
    xpos + width, ypos
}
win:draw("triangle_strip", coords)

ypos = ypos + ystep

-- draw triangle fan
win:setfg(0.5, 1, 1)
win:print("tri_fan", xpos, ypos - theight)
coords = {
    xpos, ypos,
    xpos, ypos + height,
    xpos + width, ypos + height,
    xpos + width, ypos
}
win:draw("triangle_fan", coords)

xpos = xpos + xstep
ypos = ypos0

-- draw colored line loop
win:setfg(1, 1, 1)
coords = {
    xpos, ypos,
    xpos, ypos+height,
    xpos + width, ypos + height,
    xpos + width, ypos
}
cols = {
    1,0,0,1,
    0,1,0,1,
    1,1,1,1,
    1,1,0,1
}
win:print("col_loop", xpos, ypos - theight)
win:draw("line_loop", coords, nil, cols)

ypos = ypos + ystep

-- draw colored tri fan
win:setfg(1, 1, 1)
coords = {
    xpos, ypos,
    xpos, ypos+height,
    xpos + width, ypos + height,
    xpos + width, ypos
}
cols = {
    1,0,0,1,
    0,1,0,1,
    1,1,1,1,
    1,1,0,1
}
win:print("col_fan", xpos, ypos - theight)
win:draw("triangle_fan", coords, nil, cols)

ypos = ypos + ystep

-- draw textured tri fan
win:setfg(1, 1, 1)
win:settexture(img)
coords = {
    xpos, ypos,
    xpos, ypos+height,
    xpos + width, ypos + height,
    xpos + width, ypos
}
texc = {
    0, 1,
    0, 0,
    1, 0,
    1, 1
}
win:print("tex_fan", xpos, ypos - theight)
win:draw("triangle_fan", coords, texc)

ypos = ypos + ystep

-- draw colored textured tri fan
win:setfg(1, 1, 1)
win:print("col_tex_fan", xpos, ypos - theight)
win:settexture(img)
coords = {
    xpos, ypos,
    xpos, ypos+height,
    xpos + width, ypos + height,
    xpos + width, ypos
}
texc = {
    0, 1,
    0, 0,
    1, 0,
    1, 1
}
cols = {
    1,0,0,1,
    0,1,0,1,
    1,1,1,1,
    1,1,0,1
}
win:draw("triangle_fan", coords, texc, cols)
win:settexture()

ypos = ypos + ystep

-- draw generated rectangle
win:setfg(1, 0.5, 1)
win:print("gen_rect", xpos, ypos - theight)
coords, texc = gfx.gentexturedrect(xpos, ypos, width, height)
win:draw("triangles", coords)

ypos = ypos + ystep

-- draw generated textured rectangle
win:setfg(1, 1, 1)
win:print("gen_tex_rect", xpos, ypos - theight)
win:settexture(img)
coords, texc = gfx.gentexturedrect(xpos, ypos, width, height)
win:draw("triangles", coords, texc)
win:settexture()

win:seteventhandler(function(w, e)
    if e.key == "Escape" then
        w:close()
        return true
    end
end)
gfx.mainloop()
