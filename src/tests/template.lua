-- test/template.lua
-- template for tests
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require"gfx"
win = gfx.window("test template")

-- here be the tests
fsize = 20
msg = "Nothing to see here, this is just a template!"
ww, wh = win:getsize()
sw, sh = win:getfont():rendersize(msg, fsize)
win:setfont(nil, fsize)
x = math.floor((ww - sw) / 2)
y = math.floor((wh - sh) / 2)
win:print(msg, x, y)
-- end of tests

win:seteventhandler(function(w, e)
	if e.key == "Escape" then
		w:close()
		return true
	end
end)
gfx.mainloop()
