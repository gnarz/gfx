-- test/event.lua
-- tests the event handling
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require "gfx"

function dump(...)
	local seen = {}
	local function dumpval(val, name, pfx)
		pfx = pfx or ""
		local t = type(val)
		if name ~= nil then
			io.write(name, " = ")
		end
		if t == "string" then
			io.write('"', string.gsub(val, '"', '\\"'), '"')
		elseif t == "table" then
			io.write('<',tostring(val),'>', " {\n")
			local lpfx = "  " .. pfx
			for k, v in pairs(val) do
				io.write(lpfx, '[', tostring(k), "] = ")
				dumpval(v, nil, lpfx)
			end
			io.write(pfx, '}')
		elseif t == "boolean" or t == "number" then
			io.write(tostring(val))
		else
			io.write('<',tostring(val),'>')
		end
		io.write("\n")
	end

	for n = 1, select('#', ...) do
		local a = select(n, ...)
		dumpval(a, n)
		n = n + 1
	end
end

w1 = gfx.window("event test 1", 300, 200, "resizable")
w2 = gfx.window("event test 2", 300, 200, "resizable")

function make_window_event_handler(name)
	return function(win, evt)
		if evt.type == "window" then
			print("vvvvvv-"..name.."-vvvvvv")
			dump(evt)
			print("^^^^^^-"..name.."-^^^^^^")
			if (evt.which == "show") or (evt.which == "resize") then
				print(evt.which)
				win:clear()
			elseif evt.which == "close" then
				print("closing")
				win:close()
			end
			return true
		end
		return false
	end
end

w1:seteventhandler(make_window_event_handler("WINDOW 1"))
w2:seteventhandler(make_window_event_handler("WINDOW 2"))

repeat
	evt = gfx.pollevent(1000)
	if evt then
		print("vvvvv-MAINLOOP-vvvvv")
		dump(evt)
		print("^^^^^-MAINLOOP-^^^^^")
	end
until evt and evt.type == "quit"
