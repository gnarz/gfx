-- test/transform.lua
-- tests the transformations on different objects
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require "gfx"
win = gfx.window("transformation tests", 800, 600, "single")

local steps = 10

win:clear()

function test_rotate(step)
	local a = math.pi / steps * step
	win:settransform():translate(200, 150):rotate(a):translate(-50,-50)
	win:rectangle(0,0,100,100, false)
end

function test_scale_rotate(step)
	local a = math.pi / steps * step
	win:settransform():translate(200, 450):scale(step/5):rotate(a):translate(-50,-50)
	win:rectangle(0,0,100,100, false)
end

function test_image(step)
	local img = gfx.loadimage("tests/image2.png")
	local a = math.pi / steps * step
	win:settransform():translate(700 - step * 10, 150):scale(step):rotate(a)
	win:putimage(img, 0, 0)
end

function test_font(step)
	local a = math.pi / steps * step
	win:settransform():translate(750, 350 + step * 10):scale(step):rotate(a)
	win:print("Hallo", 0, 0)
end

for step=1, steps do
	test_rotate(step)
	test_scale_rotate(step)
	test_image(step)
	test_font(step)
end

win:update()

win:seteventhandler(function(w, e)
	if e.key == "Escape" then
		w:close()
		return true
	end
end)
gfx.mainloop()
