-- test/timer.lua
-- tests the timer handling
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require "gfx"

win = gfx.window("Timer test", 300, 200)
win:print("Timer Test", 0, 0)

win:seteventhandler(
	function(w, e)
		if e then
			if e.type == "timer" then
				local ok = gfx.settimer(e.interval, e.data)
				print("window timer hit, re-registering ", ok and "ok" or "failed")
				return true
			elseif e.type == "key" and e.key == "Escape" then
				w:close()
				return true
			end
		end
	end
)

gfx.settimer(1000, win)
if gfx.settimer(1000, win) == true then
	error("immediately re-setting the window timer unexpectedly worked...")
else
	print("immediately re-setting the window timer failed, as expected")
end

gfx.settimer(1111, "Timer 1")
if gfx.settimer(1111, "Timer 1") == true then
	error("immediately re-setting timer 1 unexpectedly worked...")
else
	print("immediately re-setting timer 1 failed, as expected")
end

gfx.settimer(1333, "Timer 2")
if gfx.settimer(1222, "Timer 2") == true then
	error("immediately re-setting timer 2 unexpectedly worked...")
else
	print("immediately re-setting timer 2 failed, as expected")
end

gfx.settimer(5000, "kill1")
gfx.settimer(10000, "kill2")
gfx.settimer(15000, "kill3")
gfx.settimer(20000, "killall")

local evt
repeat
	evt = gfx.pollevent()
	if evt and evt.type == "timer" then
		if evt.data == "kill1" then
			gfx.cleartimer(win)
			print("kill1 timer hit, killing window timer")
		elseif evt.data == "kill2" then
			gfx.cleartimer("Timer 1")
			print("kill2 timer hit, killing Timer 1")
		elseif evt.data == "kill3" then
			gfx.cleartimer("Timer 2")
			print("kill3 timer hit, killing Timer 2")
		elseif evt.data == "killall" then
			print("killall timer hit, exiting...")
			win:close()
		else
			local ok = gfx.settimer(evt.interval, evt.data)
			print(evt.data, " hit, re-registering ", ok and "ok" or "failed")
		end
	end
until evt and evt.type == "quit"
