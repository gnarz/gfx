-- test/showhide.lua
-- tests the window show/hide methods
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require "gfx"
win = gfx.window("window show hide test")

visible = true
lastchange = 0

function swapVisible()
	if visible then
		win:hide()
	else
		win:show()
	end
	visible = not visible
	lastchange = gfx.getms()
end

repeat
	evt = gfx.pollevent()
	if evt == nil then
		if gfx.getms() - lastchange > 1000 then
			swapVisible()
			print("swap")
		end
	elseif evt.type == "window" then
		io.stdout:write("Event:\t", evt.type, "\t", evt.which)
		if evt.which == "focus" then
			if evt.lost then
				print("", "lost")
			else
				print("", "gained")
			end
		else
			print("")
			if evt.which == "show" then
				win:clear()
			end
		end
	elseif evt.type == "key" then
		if evt.key == "Escape" then
			gfx.exit()
		end
	end
until evt and evt.type == "quit"
