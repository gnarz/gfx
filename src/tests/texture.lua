-- test/texture.lua
-- tests the image and texture handling and rendering
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require "gfx"
win = gfx.window("texture test", 800, 600, "single")
txt = gfx.loadimage("tests/image1.png")
spr = gfx.loadimage("tests/image2.png")

myshader = gfx.shader [[
	in vec2 itexc;
	uniform sampler2D texture0, texture1;
	out vec4 fragColor;
	void main() {
			fragColor = texture(texture0, itexc) * texture(texture1, itexc);
	}
]]

coord, texc = gfx.gentexturedrect(0, 0, 100, 100)

cols = {
	1,1,1,1,
	1,1,0,1,
	1,0,1,1,
	1,1,1,1,
	1,0,1,1,
	0,1,1,1
}

win:clear()
win:settransform():translate(10, 10)

-- basic functionality: rectangle with texture, texture and color, texture and colors per edge
win:settexture(txt)
win:draw("triangles", coord, texc)
win:setfg(1,0,0)
win:translate(100,0)
win:draw("triangles", coord, texc)
win:setfg(1,1,1)
win:translate(100,0)
win:draw("triangles", coord, texc, cols)
win:settransform():translate(10, 10)
win:rectangle(0,0,300,100,false)

-- image grabbing. Needs to update for this to work
win:update()
local img = win:grabimage(10,10,300,100)
win:settransform():translate(0,150):scale(6,2)
win:settexture(img)
win:draw("triangles", coord, texc)

-- putimage
win:putimage(txt, 500, 10)
win:putimage(spr, 500, 50)

-- custom shader, multiple textures
win:settransform():translate(0, 360):scale(2)
win:setshader(myshader)
win:settexture(txt, 0)
win:settexture(spr, 1)
win:draw("triangles", coord, texc)

win:update()

win:seteventhandler(function(w, e)
	if e.key == "Escape" then
		w:close()
		return true
	end
end)
gfx.mainloop()
