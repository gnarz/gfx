-- test/speed2.lua
-- tests the speed of text drawing functions
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

utf8 = require "utf8"
gfx = require"gfx"

function draw(win, size)
	win:clear(0, 0, 0)
	win:setfg(0.5, 0.5, 0.5)
	local ww, wh = win:getsize()
	local fnt, osize = win:getfont()
	win:setfont(fnt, size)
	local str = {}
	local fw = math.floor(ww / fnt:rendersize("M", size))
	local c = string.byte("!")
	while #str <= fw do
		if fnt:hasglyph(c) then
			str[#str+1] = utf8.char(c)
		end
		c = c + 1
	end
	str = table.concat(str)
	local rows = 0
	for y = 0, wh - size, size do
		win:print(str, 0, y)
		rows = rows + 1
	end
	win:setfg(1, 1, 1)
	win:setfont()
	return rows * #str
end

win = gfx.window("speed test", 800, 600, "single")

local t0 = gfx.getms()
local nr = draw(win, 16)
local t1 = gfx.getms()

win:print(string.format("%d cells drawn in %d ms", nr, t1 - t0), 0, 0)
win:update()

win:seteventhandler(function(w, e)
	if e.key == "Escape" then
		w:close()
		return true
	end
end)
gfx.mainloop()
