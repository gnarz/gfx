-- test/font.lua
-- tests the font handling and text rendering
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

utf8 = require "utf8"
gfx = require"gfx"
win = gfx.window("font test", 800, 600, "single")
fnt1 = win:getfont()
fnt2 = gfx.loadfont("tests/unicode_8x12.ttf")

glyphs1 = {}
glyphs2 = {}
for i=0, 0xffff do
	local ch = utf8.char(i)
	if fnt1:hasglyph(i) then
		glyphs1[#glyphs1+1] = ch
	end
	if fnt2:hasglyph(ch) then
		glyphs2[#glyphs2+1] = ch
	end
end

w, h = win:getsize()
fsize1 = math.floor(math.sqrt(w * h / #glyphs1 / 2))
if fsize1 < 10 then fsize1 = 10 end
fsize2 = math.floor(math.sqrt(w * h / #glyphs2 / 2))
if fsize2 < 10 then fsize2 = 10 end
fsize = fsize1 < fsize2 and fsize1 or fsize2

i = 1
for y=1, math.floor(h/fsize) do
	for x=1, math.floor(w/fsize/2) do
		if i <= #glyphs1 then
			win:setfont(fnt1, fsize)
			win:setfg(1, 1, 1)
			c = glyphs1[i]
			cw, ch = fnt1:rendersize(c, size)
			xofs = math.floor((fsize - cw) / 2)
			win:print(c, (x-1)*fsize+xofs, (y-1)*fsize)
		end
		if i <= #glyphs2 then
			win:setfont(fnt2, fsize)
			win:setfg(0.5, 0.5, 1)
			c = glyphs2[i]
			cw, ch = fnt2:rendersize(c, size)
			xofs = math.floor((fsize - cw) / 2)
			win:print(c, w/2 + (x-1)*fsize+xofs, (y-1)*fsize)
		end
		i=i+1
	end
end

win:update()

win:seteventhandler(function(w, e)
	if e.key == "Escape" then
		w:close()
		return true
	end
end)
gfx.mainloop()
