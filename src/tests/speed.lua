-- test/speed.lua
-- tests the speed of drawing functions
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require"gfx"

function draw(win, minrectsize)
	local ww, wh = win:getsize()
	local nrh = math.floor(ww / minrectsize)
	local nrv = math.floor(wh / minrectsize)
	local rw = math.floor(ww / nrh)
	local rh = math.floor(wh / nrv)
	local fnt, fsize = win:getfont()
	win:setfont(fnt, minrectsize)

	win:clear(0, 0, 0)
	for x = 1, nrh do
		local xpos = (x - 1) * rw
		for y = 1, nrv do
			local ypos = (y - 1) * rh
			win:setfg(x / nrh, 0, y / nrv)
			win:rectangle(xpos, ypos, rw, rh, true)
			win:setfg(0, 0, 0)
			win:rectangle(xpos, ypos, rw, rh, false)

			win:setfg(1 - x / nrh, 0.5, 1 - y / nrv)
			local cw, ch = fnt:rendersize("X", minrectsize)
			win:print("X", xpos + (rw - cw) / 2, ypos + (rh - ch) / 2)
		end
	end
	win:update()
	win:setfont(fnt, fsize)

	return nrh * nrv
end

win = gfx.window("speed test", 800, 600, "single")

local t0 = gfx.getms()
local nr = draw(win, 32)
local t1 = gfx.getms()

win:print(string.format("%d cells drawn in %d ms", nr, t1 - t0), 0, 0)
win:update()

win:seteventhandler(function(w, e)
	if e.key == "Escape" then
		w:close()
		return true
	end
end)
gfx.mainloop()
