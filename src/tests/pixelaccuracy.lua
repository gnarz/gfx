-- test/pixelaccuracy.lua
-- tests the pixel accuracy of the projection
-- Gunnar Zötl <gz@tset.de> 2017
-- Released under MIT/X11 license. See file LICENSE for details.

gfx = require "gfx"
win = gfx.window("pixel accuracy test", 100, 100)

win:clear(1,0,0)
for x=0, 99 do
	win :point(x, x)
		:point(x, 0)
		:point(x,99)
		:point(0,x)
		:point(99,x)
end

img = win:grabimage()

win:seteventhandler(function(w, e)
	if e.key == "Escape" then
		w:close()
		return true
	end
end)
gfx.mainloop()
