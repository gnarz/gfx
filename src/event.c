/* event.c
 *
 * event handling for gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"

/*** Module
 * Name: gfx
 */

/*** Object
 * Name: event
 * not a real object, mind you, just a table returned by gfx.pollevent()
 * common fields:
 * - type: "window", "key", "mouse", "quit"
 * - timestamp
 * - window
 * - which (subevent)
 * 
 * # Window Events #
 * - "hide" is sent after the window has been hidden
 * - "show" is sent after the window has been shown
 * - "resize" -> adds fields "width", "height"
 * - "focus" -> adds fields "gained" or "lost", boolean
 * - "close" is sent when the window is about to be closed
 *
 * # Key events #
 * - "up"
 * - "repeat"
 * - "down"
 * additional fields for key events:
 * - "code" -> keycode
 * - "scancode"
 * - "key" -> key name
 * - active modifiers as boolean fields: "lshift", "rshift", "shift", "lctrl", "rctrl", "ctrl", "lalt", "ralt", "alt", "lgui", "rgui", "gui", "num", "caps", "mode"
 *
 * # Mouse events #
 * - "enter"
 * - "leave"
 * - "move" -> adds fields "x", "y", "xrel", "yrel"
 * - "button" -> adds fields "button", "x", "y", "state" = "up" or "down", "clicks"
 * - "wheel" -> adds fields "x", "y"
 */
static void gfx_createEvent(lua_State *L, const char *type, const char *which, uint32_t timestamp, uint32_t windowid)
{
	lua_newtable(L);
	lua_pushliteral(L, "type");
	lua_pushstring(L, type);
	lua_rawset(L, -3);
	lua_pushliteral(L, "timestamp");
	lua_pushinteger(L, timestamp);
	lua_rawset(L, -3);
	lua_pushliteral(L, "window");
	if (gfx_findWindow(L, windowid)) {
		lua_rawset(L, -3);
	} else {
		lua_pop(L, 1);
	}
	if (which) {
		lua_pushliteral(L, "which");
		lua_pushstring(L, which);
		lua_rawset(L, -3);
	}
}

static int gfx_createWindowEvent(lua_State *L, SDL_Event *evt)
{
	SDL_WindowEvent *w = &evt->window;
	
	switch (w->event) {
		case SDL_WINDOWEVENT_HIDDEN:
			gfx_createEvent(L, "window", "hide", w->timestamp, w->windowID);
			break;
		// MACOS X does not generate the exposed event, whereas linux seems
		// to generate the shown event before the window is shown...
		#ifdef __APPLE__
		case SDL_WINDOWEVENT_SHOWN:
		#else
		case SDL_WINDOWEVENT_EXPOSED:
		#endif
			gfx_createEvent(L, "window", "show", w->timestamp, w->windowID);
			break;
		case SDL_WINDOWEVENT_RESIZED:
		case SDL_WINDOWEVENT_SIZE_CHANGED:
		case SDL_WINDOWEVENT_MAXIMIZED:
			gfx_createEvent(L, "window", "resize", w->timestamp, w->windowID);
			lua_pushliteral(L, "width");
			lua_pushinteger(L, w->data1);
			lua_rawset(L, -3);
			lua_pushliteral(L, "height");
			lua_pushinteger(L, w->data2);
			lua_rawset(L, -3);
			break;
		case SDL_WINDOWEVENT_ENTER:
			gfx_createEvent(L, "mouse", "enter", w->timestamp, w->windowID);
			break;
		case SDL_WINDOWEVENT_LEAVE:
			gfx_createEvent(L, "mouse", "leave", w->timestamp, w->windowID);
			break;
		case SDL_WINDOWEVENT_FOCUS_GAINED:
			gfx_createEvent(L, "window", "focus", w->timestamp, w->windowID);
			lua_pushliteral(L, "gained");
			lua_pushboolean(L, 1);
			lua_rawset(L, -3);
			break;
		case SDL_WINDOWEVENT_FOCUS_LOST:
			gfx_createEvent(L, "window", "focus", w->timestamp, w->windowID);
			lua_pushliteral(L, "lost");
			lua_pushboolean(L, 1);
			lua_rawset(L, -3);
			break;
		case SDL_WINDOWEVENT_CLOSE:
			gfx_createEvent(L, "window", "close", w->timestamp, w->windowID);
			break;
		// unused events:
		//case SDL_WINDOWEVENT_MINIMIZED: puts("event: minimized"); return 0;
		//case SDL_WINDOWEVENT_RESTORED: puts("event: restored"); return 0;
		//case SDL_WINDOWEVENT_MOVED: puts("event: moved"); return 0;
		default:
			return 0;
	}
	return 1;
}

static void gfx_pushKeyMods(lua_State *L, uint16_t mod)
{
	int shift = 0, ctrl = 0, alt = 0, gui = 0;
	if (mod & KMOD_LSHIFT) {
		lua_pushliteral(L, "lshift");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
		shift = 1;
	}
	if (mod & KMOD_RSHIFT) {
		lua_pushliteral(L, "rshift");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
		shift = 1;
	}
	if (mod & KMOD_LCTRL) {
		lua_pushliteral(L, "lctrl");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
		ctrl = 1;
	}
	if (mod & KMOD_RCTRL) {
		lua_pushliteral(L, "rctrl");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
		ctrl = 1;
	}
	if (mod & KMOD_LALT) {
		lua_pushliteral(L, "lalt");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
		alt = 1;
	}
	if (mod & KMOD_RALT) {
		lua_pushliteral(L, "ralt");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
		alt = 1;
	}
	if (mod & KMOD_LGUI) {
		lua_pushliteral(L, "lgui");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
		gui = 1;
	}
	if (mod & KMOD_RGUI) {
		lua_pushliteral(L, "rgui");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
		gui = 1;
	}
	if (mod & KMOD_NUM) {
		lua_pushliteral(L, "num");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
	}
	if (mod & KMOD_CAPS) {
		lua_pushliteral(L, "caps");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
	}
	if (mod & KMOD_MODE) {
		lua_pushliteral(L, "mode");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
	}
	if (shift) {
		lua_pushliteral(L, "shift");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
	}
	if (ctrl) {
		lua_pushliteral(L, "ctrl");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
	}
	if (alt) {
		lua_pushliteral(L, "alt");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
	}
	if (gui) {
		lua_pushliteral(L, "gui");
		lua_pushboolean(L, 1);
		lua_rawset(L, -3);
	}
}

static int gfx_createKeyboardEvent(lua_State *L, SDL_Event *evt)
{
	SDL_KeyboardEvent *k = &evt->key;
	gfx_createEvent(L, "key", 0, k->timestamp, k->windowID);
	const char *which = "up";
	if (k->state == SDL_PRESSED) {
		which = k->repeat ? "repeat" : "down";
	}
	lua_pushliteral(L, "which");
	lua_pushstring(L, which);
	lua_rawset(L, -3);
	lua_pushliteral(L, "code");
	lua_pushinteger(L, k->keysym.sym);
	lua_rawset(L, -3);
	lua_pushliteral(L, "scancode");
	lua_pushinteger(L, k->keysym.scancode);
	lua_rawset(L, -3);
	lua_pushliteral(L, "key");
	lua_pushstring(L, SDL_GetKeyName(k->keysym.sym));
	lua_rawset(L, -3);
	gfx_pushKeyMods(L, k->keysym.mod);
	return 1;
}

static int gfx_createMouseMotionEvent(lua_State *L, SDL_Event *evt)
{
	SDL_MouseMotionEvent *m = &evt->motion;
	gfx_createEvent(L, "mouse", "move", m->timestamp, m->windowID);
	lua_pushliteral(L, "x");
	lua_pushinteger(L, m->x);
	lua_rawset(L, -3);
	lua_pushliteral(L, "y");
	lua_pushinteger(L, m->y);
	lua_rawset(L, -3);
	lua_pushliteral(L, "xrel");
	lua_pushinteger(L, m->xrel);
	lua_rawset(L, -3);
	lua_pushliteral(L, "yrel");
	lua_pushinteger(L, m->yrel);
	lua_rawset(L, -3);
	return 1;
}

static int gfx_createMouseButtonEvent(lua_State *L, SDL_Event *evt)
{
	SDL_MouseButtonEvent *m = &evt->button;
	gfx_createEvent(L, "mouse", "button", m->timestamp, m->windowID);
	lua_pushliteral(L, "button");
	lua_pushinteger(L, m->button);
	lua_rawset(L, -3);
	lua_pushliteral(L, "state");
	lua_pushstring(L, m->state == SDL_PRESSED ? "down" : "up");
	lua_rawset(L, -3);
	lua_pushliteral(L, "clicks");
	lua_pushinteger(L, m->clicks);
	lua_rawset(L, -3);
	lua_pushliteral(L, "x");
	lua_pushinteger(L, m->x);
	lua_rawset(L, -3);
	lua_pushliteral(L, "y");
	lua_pushinteger(L, m->y);
	lua_rawset(L, -3);
	return 1;
}

static int gfx_createMouseWheelEvent(lua_State *L, SDL_Event *evt)
{
	SDL_MouseWheelEvent *m = &evt->wheel;
	gfx_createEvent(L, "mouse", "wheel", m->timestamp, m->windowID);
	int32_t x = m->x, y = m->y;
	#if SDL_VERSION_ATLEAST(2,0,4)
	if (m->direction == SDL_MOUSEWHEEL_FLIPPED) {
		x *= -1;
		y *= -1;
	}
	#endif
	lua_pushliteral(L, "x");
	lua_pushinteger(L, x);
	lua_rawset(L, -3);
	lua_pushliteral(L, "y");
	lua_pushinteger(L, y);
	lua_rawset(L, -3);
	return 1;
}

static int gfx_createTimerEvent(lua_State *L, SDL_Event *evt)
{
	SDL_UserEvent *u = &evt->user;
	gfx_createEvent(L, "timer", 0, u->timestamp, 0);
	lua_pushinteger(L, (uint32_t) u->code);
	lua_setfield(L, -2, "interval");
	if (!gfx_getTimerData(L, u->data1)) {
		lua_pop(L, 1);
		return 0;
	}
	gfx_window *win = gfx_toWindow(L, -1);
	if (win) {
		u->windowID = SDL_GetWindowID(win->win);
	}
	lua_setfield(L, -2, "data");
	gfx_unregisterTimer(L, u->data1);
	return 1;
}

static int gfx_createQuitEvent(lua_State *L, SDL_Event *evt)
{
	gfx_createEvent(L, "quit", 0, evt->quit.timestamp, 0);
	return 1;
}

int gfx_pollEvent(lua_State *L, int wait, uint32_t timeout)
{
	int wid_isset = 0;
	uint32_t windowid = 0;

#define setWindowId(id) do { wid_isset = 1; windowid = id; } while (0)

	SDL_Event evt;
	int ok = 0;
	if (wait && timeout) {
		ok = SDL_WaitEventTimeout(&evt, timeout);
	} else if (wait) {
		ok = SDL_WaitEvent(&evt);
	} else {
		ok = SDL_PollEvent(&evt);
	}
	if (ok == 0) {
		return 0;
	}
	int pushed = 0;
	switch (evt.type) {
		case SDL_WINDOWEVENT:
			pushed = gfx_createWindowEvent(L, &evt);
			setWindowId(evt.window.windowID);
			break;
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			pushed = gfx_createKeyboardEvent(L, &evt);
			setWindowId(evt.key.windowID);
			break;
		case SDL_MOUSEMOTION:
			pushed = gfx_createMouseMotionEvent(L, &evt);
			setWindowId(evt.motion.windowID);
			break;
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			pushed = gfx_createMouseButtonEvent(L, &evt);
			setWindowId(evt.button.windowID);
			break;
		case SDL_MOUSEWHEEL:
			pushed = gfx_createMouseWheelEvent(L, &evt);
			setWindowId(evt.wheel.windowID);
			break;
		case SDL_QUIT:
			pushed = gfx_createQuitEvent(L, &evt);
			break;
		default:;
			/* this is a user event, not constant, so can't be a switch case */
			if (evt.type == GFX_TIMEREVENT) {
				pushed = gfx_createTimerEvent(L, &evt);
				if (evt.user.windowID) {
					setWindowId(evt.user.windowID);
				}
				break;
			}
		/*
		SDL_TextEditingEvent
		SDL_TextInputEvent
		SDL_JoyAxisEvent
		SDL_JoyBallEvent
		SDL_JoyHatEvent
		SDL_JoyButtonEvent
		SDL_JoyDeviceEvent
		SDL_ControllerAxisEvent
		SDL_ControllerButtonEvent
		SDL_ControllerDeviceEvent
		SDL_AudioDeviceEvent
		SDL_UserEvent
		SDL_SysWMEvent
		SDL_TouchFingerEvent
		SDL_MultiGestureEvent
		SDL_DollarGestureEvent
		SDL_DropEvent
		*/
	}
	if (pushed && wid_isset && gfx_findWindow(L, windowid)) {
		if (gfx_window_invokehandler(L) != 0) {
			lua_pop(L, 1);
			return 0;
		}
	}
	return pushed;

#undef setWindowId
}

/*** Function
 * Name: pollevent
 * Synopsis: event = gfx.pollevent() or gfx.pollevent(true) or gfx.pollevent(timeout)
 */
static int gfx_pollevent(lua_State *L)
{
	gfx_check_nargs(0, 0, 1);
	int wait = 0;
	uint32_t timeout = 0;
	if (lua_gettop(L) > 0) {
		if (lua_isboolean(L, 1) || lua_isnil(L, 1)) {
			if (lua_toboolean(L, 1) == 1) {
				wait = 1;
			}
		} else if (lua_isinteger(L, 1)) {
			wait = 1;
			timeout = lua_tointeger(L, 1);
		} else {
			return luaL_argerror(L, 1, "expected boolean or integer");
		}
		lua_pop(L, lua_gettop(L));
	}

	return gfx_pollEvent(L, wait, timeout);
}

static const luaL_Reg gfx_event_funcs[] = {
	{"pollevent", gfx_pollevent},
	{0, 0}
};

int gfx_registerEventFunctions(lua_State *L)
{
	luaL_setfuncs(L, gfx_event_funcs, 0);
	return 1;
}
