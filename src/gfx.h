/* gfx.h
 *
 * common header file for all of gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#ifndef gfx_h
#define gfx_h

#include "SDL2/SDL.h"
#include "GL/glew.h"
#include <stdint.h>

#ifndef DEBUGGING
#define NDEBUG
#endif
#include <assert.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#if LUA_VERSION_NUM == 501
/* very raw approximations of some newer functions for lua 5.1 */
#define luaL_newlib(L,funcs) lua_newtable(L); luaL_register(L, NULL, funcs)
/* ignores nup, so can only be used for functions without shared upvalues */
#define luaL_setfuncs(L,funcs,x) luaL_register(L, NULL, funcs)
/* only equivalent of the uservalue is a table */
#define lua_setuservalue(L, idx) lua_setfenv(L, idx)
/* on lua 5.1, an empty table is returned if no setfenv() call preceded this.
 * On later luas, nil is returned when the uservalue was not set. Also, lua
 * 5.3 returns the type of the gotten value, we simulate that by returning
 * LUA_TTABLE (the only thing lua_getfenv can return) */
#define lua_getuservalue(L, idx) (lua_getfenv(L, idx), LUA_TTABLE)
/* lua 5.3 returns the type for these functions, so we mimic that here */
#define lua_getfield(L, idx, what) (lua_getfield(L, idx, what), lua_type(L, -1))
#define lua_rawgeti(L, idx, pos) (lua_rawgeti(L, idx, pos), lua_type(L, -1))
/* simulate lua_isinteger. The range is obviously a bit more limited, but
 * for our purpose this will do */
#define lua_isinteger(L, idx) (lua_isnumber(L, idx) && (lua_tonumber(L, idx) == floor(lua_tonumber(L, idx))))
/* strange but true, lua 5.2+ pushes the result of the length op to the
 * stack. */
#define lua_len(L, idx) lua_pushnumber(L, lua_objlen(L, idx))
#define LUA_OK 0

#elif LUA_VERSION_NUM == 502
/* very raw approximations of some newer functions for lua 5.2 */
/* lua 5.2 also has no integers */
#define lua_isinteger(L, idx) (lua_isnumber(L, idx) && (lua_tonumber(L, idx) == floor(lua_tonumber(L, idx))))
/* lua 5.3 returns the type for these functions, so we mimic that here */
#define lua_getuservalue(L, idx) (lua_getuservalue(L, idx), lua_type(L, -1))
#define lua_getfield(L, idx, what) (lua_getfield(L, idx, what), lua_type(L, -1))
#define lua_rawgeti(L, idx, pos) (lua_rawgeti(L, idx, pos), lua_type(L, -1))
#endif

#define GFX_WRONG_NARGS_ERR "incorrect number of arguments"
#define gfx_check_nargs(ofs, min, max) do { int nargs = lua_gettop(L) - ofs; if (nargs < min || nargs > max) { if (min == max) { luaL_error(L, GFX_WRONG_NARGS_ERR ", expected %d", min); } else { luaL_error(L, GFX_WRONG_NARGS_ERR ", expected %d - %d", min, max); } } } while (0)

/* misc types and gl support */
typedef float gfx_matrix[16];
typedef struct { float x, y; } gfx_vec2;
typedef struct { float r, g, b, a; } gfx_color;
typedef struct { float x, y, w, h; } gfx_rect;

/* matrix.c */

gfx_matrix *gfx_matrix_loadidentity(gfx_matrix *m);
gfx_matrix *gfx_matrix_ortho(gfx_matrix *m, int width, int height);
gfx_matrix *gfx_matrix_rotate2d(gfx_matrix *m, float a);
gfx_matrix *gfx_matrix_translate2d(gfx_matrix *m, float x, float y);
gfx_matrix *gfx_matrix_scale2d(gfx_matrix *m, float x, float y);

/* gfx.c */

typedef void (*gfx_finalizer)(lua_State *L);
extern void gfx_registerFinalizer(gfx_finalizer f);
extern int gfx_tableLen(lua_State *L, int idx);
extern int gfx_sdlError(lua_State *L, const char *msg);
extern int gfx_checkGLerror(lua_State *L, const char *msg);
extern int gfx_setProperty(lua_State *L, int idx, int val, const char *prop);
extern int gfx_getProperty(lua_State *L, int idx, const char *prop);

/* helperobject.c */

void* gfx_pushHelper(lua_State *L, const char *type, int size);
void gfx_createHelperObject(lua_State *L, const char *type);

/* event.c */
int gfx_pollEvent(lua_State *L, int wait, uint32_t timeout);
extern int gfx_registerEventFunctions(lua_State *L);

/* timer.c */

extern uint32_t GFX_TIMEREVENT;
#define GFX_TIMER_REGISTRY "gfx_timer_registry"

extern void gfx_unregisterTimer(lua_State *L, void *param);
extern int gfx_getTimerData(lua_State *L, void *param);
extern int gfx_registerTimerFunctions(lua_State *L);

/* window.c */
#define GFX_WINDOW "gfx_window"
#define GFX_WINDOW_REGISTRY "gfx_window_registry"

#define WINDOW_PROP_HANDLER "handler"
#define WINDOW_PROP_FRAMEBUFFER "framebuffer"

typedef enum {
	DIRECT,
	SINGLE,
	DOUBLE
} gfx_rendering_mode;

typedef struct {
	uint32_t id;
	SDL_Window *win;
	SDL_GLContext ctx;
	gfx_rendering_mode mode;
	gfx_matrix projection;
	gfx_matrix modelview;
	gfx_color fgcolor, bgcolor;
	GLuint vao, vbo, fbo;
	int vbosize;
	// also current texture, shader, render target, in lua_uservalue
} gfx_window;

extern gfx_window* gfx_checkWindow(lua_State *L, int index);
extern gfx_window* gfx_toWindow(lua_State *L, int index);
extern gfx_window* gfx_checkAndUseWindow(lua_State *L, int index);
extern int gfx_findWindow(lua_State *L, uint32_t id);
extern int gfx_window_invokehandler(lua_State *L);

extern int gfx_registerWindowFunctions(lua_State *L);

/* globject.c */

typedef void (*gfx_globject_destructor)(GLuint);

typedef struct {
	GLuint id;
	gfx_globject_destructor destroy;
} gfx_globject;

extern gfx_globject* gfx_pushGLObject(lua_State *L, const char *objtype, gfx_globject_destructor destroy);
extern int gfx_newGLObject(lua_State *L, const char *objtype, const luaL_Reg *methods);

/* shader.c */

#define WINDOW_PROP_SHADER "shader"

#define GFX_POS_COORDS 0
#define GFX_POS_TEXCS 1
#define GFX_POS_COLS 2

#define GFX_SHADER "gfx_shader"
#define GFX_SHADER_DEFAULT_VERSION "#version 150 core"

#define gfx_checkShader(L, i) luaL_checkudata(L, i, GFX_SHADER)
#define gfx_pushShader(L) gfx_pushGLObject(L, GFX_SHADER, glDeleteProgram)

typedef gfx_globject gfx_shader;

#define GLSL(version, src) "#version " #version " core\n" #src

#define GFX_MATRIX_PROJECTION "projection_matrix"
#define GFX_MATRIX_MODELVIEW "modelview_matrix"

extern GLuint gfx_shaderBuild(lua_State *L, const char *frag_shader);
extern void gfx_shaderSetUniformMatrix(GLuint id, const char *which, gfx_matrix *m);
extern void gfx_shaderSetUniformInt(GLuint id, const char *name, int val);
extern void gfx_shaderSetUniformVec2(GLuint id, const char *name, gfx_vec2 v2);
extern void gfx_shaderSetUniformColor(GLuint id, const char *name, gfx_color col);
extern GLuint gfx_getShaderId(lua_State *L, int idx);

extern int gfx_registerShaderFunctions(lua_State *L);
extern int gfx_registerShaderMethods(lua_State *L);

/* draw.c */

#define GFX_MATRIX "gfx_matrix"

extern int gfx_pushRectAsTable(lua_State *L, gfx_rect *rect);
extern void gfx_rectFromTable(lua_State *L, int pos, gfx_rect *rect);

extern void gfx_makeTexturedRectangle(const gfx_rect *r, const gfx_rect *t, gfx_vec2 *coords, gfx_vec2 *texcs);
extern int gfx_drawstuff(lua_State *L, int idx, GLenum mode, int ncoords, gfx_vec2 *coords, gfx_vec2 *texcs, gfx_color *cols);

extern int gfx_registerDrawFunctions(lua_State *L);
extern int gfx_registerDrawMethods(lua_State *L);

/* image.c */
#define GFX_IMAGE "gfx_image"

#define WINDOW_PROP_TEXTURE "texture"
#define WINDOW_PROP_TEXTURE0 WINDOW_PROP_TEXTURE "0"
#define WINDOW_PROP_TEXTURE1 WINDOW_PROP_TEXTURE "1"
#define WINDOW_PROP_TEXTURE2 WINDOW_PROP_TEXTURE "2"
#define WINDOW_PROP_TEXTURE3 WINDOW_PROP_TEXTURE "3"
#define WINDOW_PROP_TEXTURE4 WINDOW_PROP_TEXTURE "4"
#define WINDOW_PROP_TEXTURE5 WINDOW_PROP_TEXTURE "5"
#define WINDOW_PROP_TEXTURE6 WINDOW_PROP_TEXTURE "6"
#define WINDOW_PROP_TEXTURE7 WINDOW_PROP_TEXTURE "7"
#define WINDOW_PROP_TEXTURE8 WINDOW_PROP_TEXTURE "8"
#define WINDOW_PROP_TEXTURE9 WINDOW_PROP_TEXTURE "9"

#define gfx_checkImage(L, i) luaL_checkudata(L, i, GFX_IMAGE)

typedef gfx_globject gfx_image;

extern int gfx_imageSize(gfx_image *img, int *w, int *h, int *d);
extern int gfx_imageUpdate(gfx_image *img, int w, int h, uint8_t *data);
extern int gfx_pushNewImage(lua_State *L, int width, int height, int chan, uint8_t *data);

extern void gfx_setupTextures(lua_State *L, int idx);

extern int gfx_registerImageMethods(lua_State *L);
extern int gfx_registerImageFunctions(lua_State *L);

/* font.c */

#define GFX_FONT "gfx_font"
#define GFX_GLYPH "gfx_glyph"

#define GFX_DEFAULT_FONT_SIZE 16
#define GFX_DEFAULT_BITMAP_SIZE 64

#define GFX_BUILTIN_FONT "gfx_builtin_font"

#define WINDOW_PROP_FONT "font"
#define WINDOW_PROP_FONTSIZE "fontsize"

#define FONT_PROP_GLYPHMAP "glyphmap"
#define FONT_PROP_TEXTURE "texture"

typedef struct {
	int is_builtin;
	const uint8_t *data;
	struct stbtt_fontinfo *info;
	uint8_t *bitmap;
	uint16_t bm_width, bm_height;
	struct skylineEntry *skyline;
} gfx_font;

extern int gfx_registerFontMethods(lua_State *L);
extern int gfx_registerFontFunctions(lua_State *L);

#endif /* gfx_h */

