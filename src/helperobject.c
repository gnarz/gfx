/* helperobject.c
 *
 * common lua wrappers for simple structs
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"

/* gfx_pushHelper
 *
 * create a new, empty helper userdata and push it to the stack.
 */
void* gfx_pushHelper(lua_State *L, const char *type, int size)
{
	void *obj = (void*) lua_newuserdata(L, size);
	memset(obj, 0, size);
	luaL_getmetatable(L, type);
	lua_setmetatable(L, -2);
	return obj;
}

/* gfx_helper__tostring
 *
 * __tostring metamethod for the gfx_helper userdata.
 */
static int gfx_helper__toString(lua_State *L)
{
	void *obj = lua_touserdata(L, 1);
	const char *type = "(helper)";
	if (luaL_getmetafield (L, 1, "__name") != LUA_TNIL) {
		type = lua_tostring(L, -1);
		lua_pop(L, 1);
	}
	lua_pushfstring(L, "%s: %p", type, obj);
	return 1;
}

/* metamethods for the window userdata
 */
static const luaL_Reg gfx_helper_meta[] = {
	{"__tostring", gfx_helper__toString},
	{0, 0}
};

void gfx_createHelperObject(lua_State *L, const char *type)
{
	luaL_newmetatable(L, type);
	luaL_setfuncs(L, gfx_helper_meta, 0);
	lua_pop(L, 1);
}
