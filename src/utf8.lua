-- This is a single function utf8 lib emulation for lua <5.3
-- It is needed for tests/font.lua on lua < 5.3
-- Extracted from https://github.com/Stepets/utf8.lua
-- The original code is also under the MIT license, but
-- Copyright (c) 2016 Stepets

return {
	char = function(unicode)
		if unicode <= 0x7F then return string.char(unicode) end

		if (unicode <= 0x7FF) then
			local Byte0 = 0xC0 + math.floor(unicode / 0x40);
			local Byte1 = 0x80 + (unicode % 0x40);
			return string.char(Byte0, Byte1);
		end;

		if (unicode <= 0xFFFF) then
			local Byte0 = 0xE0 +  math.floor(unicode / 0x1000);
			local Byte1 = 0x80 + (math.floor(unicode / 0x40) % 0x40);
			local Byte2 = 0x80 + (unicode % 0x40);
			return string.char(Byte0, Byte1, Byte2);
		end;

		if (unicode <= 0x10FFFF) then
			local code = unicode
			local Byte3= 0x80 + (code % 0x40);
			code       = math.floor(code / 0x40)
			local Byte2= 0x80 + (code % 0x40);
			code       = math.floor(code / 0x40)
			local Byte1= 0x80 + (code % 0x40);
			code       = math.floor(code / 0x40)
			local Byte0= 0xF0 + code;

			return string.char(Byte0, Byte1, Byte2, Byte3);
		end;

		error 'Unicode cannot be greater than U+10FFFF!'
	end
}
