/* matrix.c
 *
 * transformation matrices for gfx
 *
 * Gunnar Zötl <gz@tset.de>, 2017
 * Released under MIT/X11 license. See file LICENSE for details.
 */

#include "gfx.h"

gfx_matrix *gfx_matrix_loadidentity(gfx_matrix *m)
{
	memset(m, 0, sizeof(gfx_matrix));
	(*m)[ 0] = 1.0;
	(*m)[ 5] = 1.0;
	(*m)[10] = 1.0;
	(*m)[15] = 1.0;
	return m;
}

gfx_matrix *gfx_matrix_ortho(gfx_matrix *m, int width, int height)
{
	float left = -0.5, right = width-0.5;
	float top = -0.5, bottom = height-0.5;
	float near = -1, far = 1;

	/* orthogonal projection, formula from the glu manual for gluOrtho()
	 */
	memset(m, 0, sizeof(gfx_matrix));
	(*m)[0] = 2 / (right - left);
	(*m)[5] = 2 / (top - bottom);
	(*m)[10] = -2 / (far - near);
	(*m)[12] = -(right + left) / (right - left);
	(*m)[13] = -(top + bottom) / (top - bottom);
	(*m)[14] = -(far + near) / (far - near);
	(*m)[15] = 1;
	return m;
}

gfx_matrix *gfx_matrix_rotate2d(gfx_matrix *m, float a)
{
	float sina = sin(a);
	float cosa = cos(a);
	float m0 = (*m)[0]; (*m)[0] = (*m)[0]*cosa + (*m)[4]*sina;
	float m1 = (*m)[1]; (*m)[1] = (*m)[1]*cosa + (*m)[5]*sina;
	float m2 = (*m)[2]; (*m)[2] = (*m)[2]*cosa + (*m)[6]*sina;
	float m3 = (*m)[3]; (*m)[3] = (*m)[3]*cosa + (*m)[7]*sina;
	(*m)[4] = m0*-sina + (*m)[4]*cosa;
	(*m)[5] = m1*-sina + (*m)[5]*cosa;
	(*m)[6] = m2*-sina + (*m)[6]*cosa;
	(*m)[7] = m3*-sina + (*m)[7]*cosa;
	return m;
}

gfx_matrix *gfx_matrix_translate2d(gfx_matrix *m, float x, float y)
{
	(*m)[12] = (*m)[0]*x + (*m)[4]*y + (*m)[12];
	(*m)[13] = (*m)[1]*x + (*m)[5]*y + (*m)[13];
	(*m)[14] = (*m)[2]*x + (*m)[6]*y + (*m)[14];
	(*m)[15] = (*m)[3]*x + (*m)[7]*y + (*m)[15];
	return m;
}

gfx_matrix *gfx_matrix_scale2d(gfx_matrix *m, float x, float y)
{
	(*m)[0] = (*m)[0]*x;
	(*m)[1] = (*m)[1]*x;
	(*m)[2] = (*m)[2]*x;
	(*m)[3] = (*m)[3]*x;
	(*m)[4] = (*m)[4]*y;
	(*m)[5] = (*m)[5]*y;
	(*m)[6] = (*m)[6]*y;
	(*m)[7] = (*m)[7]*y;
	return m;
}