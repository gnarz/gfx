package = "gfx"
version = "0.7-1"
source = {
   url = "http://www.tset.de/downloads/gfx-0.7-1.tar.gz"
}
description = {
   summary = "A simple 2D graphics library for lua",
   detailed = [[
		gfx is a simple graphics library for lua. It offers mainly low level
      graphics like lines, triangles and rectangles, but also generic
      rendering with custom pixel shaders and font rendering. Also, in
      addition to the standard single- and double buffered displays it also
      offers a direct rendering mode where the result of every draw
      command will be immediately visible.

	]],
   homepage = "http://www.tset.de/gfx/",
   license = "MIT",
   maintainer = "Gunnar Zötl <gz@tset.de>"
}
supported_platforms = {
   "unix"
}
dependencies = {
   "lua >= 5.1, <= 5.3"
}
external_dependencies = {
   GLEW = {
      header = "GL/glew.h"
   },
   SDL2 = {
      header = "SDL2/SDL.h"
   }
}
build = {
   type = "make",
   copy_directories = {
      "doc"
   },
   build_variables = {
      CFLAGS = "$(CFLAGS)",
      LIBFLAG = "$(LIBFLAG)",
      LUA = "$(LUA)",
      LUA_BINDIR = "$(LUA_BINDIR)",
      LUA_INCDIR = "$(LUA_INCDIR)",
      LUA_LIBDIR = "$(LUA_LIBDIR)"
   },
   install_variables = {
      INST_DIR = "$(PREFIX)",
      INST_LIBDIR = "$(LIBDIR)",
      INST_LUADIR = "$(LUADIR)"
   }
}
